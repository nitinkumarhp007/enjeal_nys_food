package com.enjeal_nys_food.parser;


public class AllAPIS {

  /*  nitin@gmail.com  (user)
    kfc@gmail.com  (Restaurtent)
    deliveryuser@gmail.com (driver)*/

    public static final String BASE_URL = "http://3.17.246.3:1201/api/";

    public static final String USERLOGIN = BASE_URL + "login";
    public static final String USER_SIGNUP = BASE_URL + "signup";
    public static final String PHONEVERIFY = BASE_URL + "sendOtp";
    public static final String VERIFY_OTP = BASE_URL + "verifyOtp";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgotPassword";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String CHANGEPASSWORD = BASE_URL + "changePassword";
    public static final String EDIT_PROFILE = BASE_URL + "editProfile";
    public static final String TERMSANDCONDITIONS = BASE_URL + "termsAndConditions";
    public static final String PRIVACYPOLICY = BASE_URL + "privacyPolicy";

    public static final String NEARBYRESTAURANTS = BASE_URL + "nearbyRestaurants";
    public static final String FOODCATEGORYLISTING = BASE_URL + "foodCategoryListing";
    public static final String FAV_UNFAV_RESTAURANT = BASE_URL + "favouriteUnfavouriteRestaurant";
    public static final String FAV_RESTAURANT = BASE_URL + "favouriteRestaurants";
    public static final String RESTAURANTDETAIL = BASE_URL + "restaurantDetail";
    public static final String ADDFOODMENUITEM = BASE_URL + "addFoodMenuItem";
    public static final String EDITFOODMENUITEM = BASE_URL + "editFoodMenuItem";
    public static final String DELETEFOODMENUITEM = BASE_URL + "deleteFoodMenuItem";
    public static final String FOODMENUITEMSLISTING = BASE_URL + "foodMenuItemsListing";
    public static final String GETRESTAURANTLIST = BASE_URL + "foodCategoryBasedRestaurants";

    public static final String ADDRESTAURTANTORDER = BASE_URL + "addRestaurantsOrder";
    public static final String REQUESTFOODLIST_USER = BASE_URL + "requestFoodListUser";
    public static final String REQUESTFOODLIST_RESTAURANT = BASE_URL + "requestFoodListRestaurant";
    public static final String ACCEPT_DECLINE_RESTAURANT_ORDER = BASE_URL + "acceptDeclineRestaurantOrder";
    public static final String FOODORDERPAYMENT = BASE_URL + "foodOrderPayment";
    public static final String USERORDERLIST = BASE_URL + "userOrdersList";
    public static final String RESTAURANTORDERLIST = BASE_URL + "restaurantOrdersList";

    public static final String DELIVERYBOYORDERSLIST = BASE_URL + "deliveryBoyOrdersList";
    public static final String ORDERPICKUPFROMRESTAURANT = BASE_URL + "orderPickupFromRestaurant";
    public static final String ORDERCOMPLETEBYDELIVERYBOY = BASE_URL + "orderCompleteByDeliveryBoy";
    public static final String GETPRICES = BASE_URL + "getAdminComission";

}
