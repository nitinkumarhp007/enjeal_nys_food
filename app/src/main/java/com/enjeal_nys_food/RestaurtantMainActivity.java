package com.enjeal_nys_food;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.enjeal_nys_food.Fragments.User.Restaurtant.HomeRestorentFragment;
import com.enjeal_nys_food.Fragments.User.Restaurtant.MenuRestorentFragment;
import com.enjeal_nys_food.Fragments.User.Restaurtant.OrderRestorentFragment;
import com.enjeal_nys_food.Fragments.User.Restaurtant.ProfileRestorentFragment;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RestaurtantMainActivity extends AppCompatActivity {

    RestaurtantMainActivity context;
    private SavePref savePref;
    BottomNavigationView navigation;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.setting)
    SwitchCompat notificationSwitch;
    String notification_code = "";
    boolean is_from_push = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurtant_main);
        context = RestaurtantMainActivity.this;
        ButterKnife.bind(this);

        savePref = new SavePref(context);

        Log.e("auth_key", savePref.getID());
        //  toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        is_from_push = getIntent().getBooleanExtra("is_from_push", false);
        if (is_from_push) {
            notification_code = getIntent().getStringExtra("notification_code");
            if (notification_code.equals("33")) {
                navigation.setSelectedItemId(R.id.navigation_order);
            } else {
                navigation.setSelectedItemId(R.id.navigation_home);
            }
        } else {
            navigation.setSelectedItemId(R.id.navigation_home);
        }

        if (savePref.getStringLatest("notification_switch").equals("1")) {
            notificationSwitch.setText("Online");
            notificationSwitch.setChecked(true);
            notificationSwitch.setTextColor(getResources().getColor(R.color.white));
        } else {
            notificationSwitch.setText("Offline");
            notificationSwitch.setChecked(false);
            notificationSwitch.setTextColor(getResources().getColor(R.color.red));
        }


        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    savePref.setStringLatest("notification_switch", "1");
                    notificationSwitch.setText("Online");
                    notificationSwitch.setTextColor(getResources().getColor(R.color.white));
                    EDIT_PROFILE("1");
                } else {
                    notificationSwitch.setText("Offline");
                    notificationSwitch.setTextColor(getResources().getColor(R.color.red));
                    EDIT_PROFILE("0");
                }
            }
        });


    }

    public void EDIT_PROFILE(String status) {
        final ProgressDialog mDialog = util.initializeProgress(context);
      //  mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ONLINE_STATUS, status);//1=online , 0=offline
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setStringLatest("notification_switch", status);
                            savePref.setAuthorization_key(body.getString("token"));
                            Toast.makeText(RestaurtantMainActivity.this, "Updated Successfully!", Toast.LENGTH_SHORT).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            notificationSwitch.setVisibility(View.VISIBLE);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    title.setText("Home");
                    loadFragment(new HomeRestorentFragment());
                    return true;
                case R.id.navigation_menu:
                    loadFragment(new MenuRestorentFragment());
                    title.setText("Menu");
                    return true;
                case R.id.navigation_order:
                    loadFragment(new OrderRestorentFragment());
                    title.setText("Orders");
                    return true;
                case R.id.navigation_profile:
                    notificationSwitch.setVisibility(View.VISIBLE);
                    loadFragment(new ProfileRestorentFragment());
                    title.setText("Profile");
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }


}


