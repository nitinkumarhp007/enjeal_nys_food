package com.enjeal_nys_food;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.enjeal_nys_food.Activities.AssignedActivity;
import com.enjeal_nys_food.Activities.ChangePasswordActivity;
import com.enjeal_nys_food.Activities.ReviewListActivity;
import com.enjeal_nys_food.Activities.SignInActivity;
import com.enjeal_nys_food.Fragments.User.ExploreFragment;
import com.enjeal_nys_food.Fragments.User.FavFoodUserFragment;
import com.enjeal_nys_food.Fragments.User.HomeUserFragment;
import com.enjeal_nys_food.Fragments.User.OrdersUserFragment;
import com.enjeal_nys_food.Fragments.User.ProfileUserFragment;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ligl.android.widget.iosdialog.IOSDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    MainActivity context;
    private SavePref savePref;
    BottomNavigationView navigation;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.setting)
    SwitchCompat setting;

    String notification_code = "", message = "";
    boolean is_from_push = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        ButterKnife.bind(this);

        savePref = new SavePref(context);

        Log.e("auth_key", savePref.getAuthorization_key());
        //  toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        is_from_push = getIntent().getBooleanExtra("is_from_push", false);
        if (is_from_push) {
            notification_code = getIntent().getStringExtra("notification_code");
            message = getIntent().getStringExtra("message");
            if (notification_code.equals("2") /*|| notification_code.equals("3")*/) {
                navigation.setSelectedItemId(R.id.navigation_order);
            } else if (notification_code.equals("3")) {
                navigation.setSelectedItemId(R.id.navigation_home);

                new IOSDialog.Builder(context)
                        .setTitle(context.getResources().getString(R.string.app_name))
                        .setCancelable(false)
                        .setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                        /* .setNegativeButton("Cancel", null)*/.show();


            } else {
                navigation.setSelectedItemId(R.id.navigation_home);

                if (notification_code.equals("7")) {
                    new IOSDialog.Builder(context)
                            .setTitle(context.getResources().getString(R.string.app_name))
                            .setCancelable(false)
                            .setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(context, AssignedActivity.class);
                            intent.putExtra("id_r", getIntent().getStringExtra("id_r"));
                            intent.putExtra("name_r", getIntent().getStringExtra("name_r"));
                            intent.putExtra("image_r", getIntent().getStringExtra("image_r"));
                            intent.putExtra("phone_r", getIntent().getStringExtra("phone_r"));

                            intent.putExtra("id_d", getIntent().getStringExtra("id_d"));
                            intent.putExtra("name_d", getIntent().getStringExtra("name_d"));
                            intent.putExtra("image_d", getIntent().getStringExtra("image_d"));
                            intent.putExtra("phone_d", getIntent().getStringExtra("phone_d"));
                            intent.putExtra("deliveryBy", getIntent().getStringExtra("deliveryBy"));
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                        }
                    })
                            /* .setNegativeButton("Cancel", null)*/.show();
                }

            }
        } else {
            if (getIntent().getBooleanExtra("from_request", false))
                navigation.setSelectedItemId(R.id.navigation_order);
            else
                navigation.setSelectedItemId(R.id.navigation_home);
        }


        Log.e("token____", SavePref.getDeviceToken(this, "token"));

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            setting.setVisibility(View.INVISIBLE);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    title.setText("Home");
                    loadFragment(new HomeUserFragment());
                    return true;
                case R.id.navigation_explore:
                    loadFragment(new ExploreFragment());
                    title.setText("Explore");
                    return true;
                case R.id.navigation_chat:
                    if (savePref.getAuthorization_key().isEmpty()) {
                        login_first(MainActivity.this);
                    } else {
                        loadFragment(new FavFoodUserFragment());
                    }
                    title.setText("Favorite");
                    return true;
                case R.id.navigation_order:
                    if (savePref.getAuthorization_key().isEmpty()) {
                        login_first(MainActivity.this);
                    } else {
                        loadFragment(new OrdersUserFragment());
                    }
                    title.setText("Orders");

                    return true;
                case R.id.navigation_profile:
                    if (savePref.getAuthorization_key().isEmpty()) {
                        login_first(MainActivity.this);
                    } else {
                        setting.setVisibility(View.INVISIBLE);
                        loadFragment(new ProfileUserFragment());
                    }
                    title.setText("Profile");
                    return true;
            }
            return false;
        }
    };

    public void login_first(Context context) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("You need to Login First").setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, SignInActivity.class);
                intent.putExtra("from_home", true);
                context.startActivity(intent);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        navigation.setSelectedItemId(R.id.navigation_home);
                    }
                }).show();
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }
}


