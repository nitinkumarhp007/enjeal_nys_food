package com.enjeal_nys_food.Fragments.User;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.enjeal_nys_food.Activities.RestorentDetailActivity;
import com.enjeal_nys_food.Activities.SignInActivity;
import com.enjeal_nys_food.Adapters.FavUserAdapter;
import com.enjeal_nys_food.Adapters.HomeuserAdapter;
import com.enjeal_nys_food.ModelClasses.UserModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.GPSTracker;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static androidx.core.content.ContextCompat.checkSelfPermission;


public class ExploreFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    Marker marker;
    private GoogleMap mMap;
    private float currentZoom = 15;
    GPSTracker gpsTracker = null;
    String latitude = "", longitude = "";
    String latitude_current = "", longitude_current = "";
    boolean hit = false;
    FusedLocationProviderClient fusedLocationClient = null;
    Location location = null;
    private ArrayList<UserModel> list;
    boolean is_move = false;

    @BindView(R.id.icon8_current)
    ImageView icon8_current;

    public ExploreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        gpsTracker = new GPSTracker(context);

        if (gpsTracker.canGetLocation()) {
            latitude_current = String.valueOf(gpsTracker.getLatitude());
            longitude_current = String.valueOf(gpsTracker.getLongitude());

            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());

            hit = true;

            if (ConnectivityReceiver.isConnected()) {
                NEARBYRESTAURANTS();
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }

            Log.e("current_location-", String.valueOf(gpsTracker.getLatitude()) + " " + String.valueOf(gpsTracker.getLongitude()));
            SupportMapFragment mapFragment = (SupportMapFragment)
                    getChildFragmentManager().findFragmentById(R.id.proselectlocationmapid);
            mapFragment.getMapAsync(this);

        }

        icon8_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng latLng = new LatLng(Double.parseDouble(latitude_current),
                        Double.parseDouble(longitude_current));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        latLng, 18);
                Log.e("ZOOM__", String.valueOf(currentZoom));
                mMap.animateCamera(location, 2100, null);
            }
        });


        return view;
    }


    private void NEARBYRESTAURANTS() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!latitude.isEmpty())
            formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
        if (!longitude.isEmpty())
            formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);
        formBuilder.addFormDataPart(Parameters.RANGE, "100");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.NEARBYRESTAURANTS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray restarurants = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < restarurants.length(); i++) {
                                JSONObject object = restarurants.getJSONObject(i);
                                UserModel userModel = new UserModel();
                                userModel.setId(object.getString("id"));
                                userModel.setEmail(object.getString("email"));
                                userModel.setImage(object.getString("image"));
                                userModel.setName(object.getString("name"));
                                userModel.setDistance(object.getString("distance"));
                                userModel.setIs_fav(object.optString("isFavourite"));
                                userModel.setOnline_status(object.optString("onlineStatus"));
                                userModel.setAvgRating(object.optString("avgRating"));

                                list.add(userModel);



                                LatLng latLng = new LatLng(Double.parseDouble(object.getString("latitude")),
                                        Double.parseDouble(object.getString("longitude")));

                                marker = mMap.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_new)));
                                marker.setTag(i);




                                if (!is_move) {
                                    is_move = true;

                                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                                            latLng, currentZoom);
                                    Log.e("ZOOM__", String.valueOf(currentZoom));
                                    mMap.animateCamera(location);


                                }
                            }

                            LatLng latLng = new LatLng(Double.parseDouble(latitude),
                                    Double.parseDouble(longitude));

                            marker = mMap.addMarker(new MarkerOptions()
                                    .position(latLng)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_3x)));
                            marker.setTag(999);


                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (gpsTracker.canGetLocation()) {
            LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
          /*  mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("")
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.pin_3x))
                    .draggable(true)
            );*/

            marker = mMap.addMarker(new MarkerOptions().draggable(true)
                    .position(latLng)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.pin_3x))
                    .draggable(true)
            );
            marker.setTag(999);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    latLng, 13);
            mMap.animateCamera(location, 2100, null);
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                int position = (int) (marker.getTag());
                // Toast.makeText(context, "Click on Marker WITH Location:" + list.get(position).getName(), Toast.LENGTH_SHORT).show();
                //Using position get Value from arraylist
                if (position != 999) {
                    Intent intent = new Intent(context, RestorentDetailActivity.class);
                    intent.putExtra("id", list.get(position).getId());
                    startActivityForResult(intent, 200);
                }

                return false;
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        EnableGPSAutoMatically();
    }


    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext());
                            if (checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    Activity#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for Activity#requestPermissions for more details.
                                return;
                            }
                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());


                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }


                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                location = locationList.get(locationList.size() - 1);
                // util.showToast(getActivity(), "lat long " + location.getLatitude());


                /*gpsTracker = new GPSTracker(context);
                //util.showToast(context, "latt " + String.valueOf(gpsTracker.getLatitude()));
                if (ConnectivityReceiver.isConnected()) {
                    AGENCIE("", true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }*/

                if (latitude.isEmpty() || latitude == null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLatitude());
                }


                if (ConnectivityReceiver.isConnected()) {
                    if (!hit) {
                        latitude_current = String.valueOf(location.getLatitude());
                        longitude_current = String.valueOf(location.getLatitude());
                        NEARBYRESTAURANTS();
                    }

                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }


                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
