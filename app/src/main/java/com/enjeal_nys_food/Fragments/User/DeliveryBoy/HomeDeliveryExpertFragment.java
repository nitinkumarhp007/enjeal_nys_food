package com.enjeal_nys_food.Fragments.User.DeliveryBoy;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enjeal_nys_food.Adapters.DeliveryExpertOngoingOrdersAdapter;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.RestaurtantMainActivity;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.GPSTracker;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncGet;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static androidx.core.content.ContextCompat.checkSelfPermission;


public class HomeDeliveryExpertFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.ongoing_orders)
    Button ongoingOrders;
    @BindView(R.id.delivered_orders)
    Button deliveredOrders;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;
    DeliveryExpertOngoingOrdersAdapter adapter;

    Context context;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    private SavePref savePref;
    @BindView(R.id.error_message)
    TextView errorMessage;

    ArrayList<RequestModel> upcoming_list;
    ArrayList<RequestModel> completed_list;


    String latitude = "", longitude = "", radius = "100000";
    GPSTracker gpsTracker = null;
    FusedLocationProviderClient fusedLocationClient = null;
    boolean hit = false;
    Location location = null;

    public HomeDeliveryExpertFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_delivery_expert, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        Log.e("dattaaaa", savePref.getAuthorization_key());

        gpsTracker = new GPSTracker(context);


        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ongoing_orders, R.id.delivered_orders})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ongoing_orders:
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                ongoingOrders.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                deliveredOrders.setTextColor(getResources().getColor(R.color.black));

                if (upcoming_list.size() > 0) {

                    adapter = new DeliveryExpertOngoingOrdersAdapter(context, upcoming_list);
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myRecyclerView.setAdapter(adapter);

                    myRecyclerView.setVisibility(View.VISIBLE);
                    errorMessage.setVisibility(View.GONE);
                } else {
                    myRecyclerView.setVisibility(View.GONE);
                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.setText("No Order Found");
                }
                break;
            case R.id.delivered_orders:
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                ongoingOrders.setTextColor(getResources().getColor(R.color.black));
                deliveredOrders.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                if (completed_list.size() > 0) {

                    adapter = new DeliveryExpertOngoingOrdersAdapter(context, completed_list);
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myRecyclerView.setAdapter(adapter);

                    myRecyclerView.setVisibility(View.VISIBLE);
                    errorMessage.setVisibility(View.GONE);
                } else {
                    myRecyclerView.setVisibility(View.GONE);
                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.setText("No Order Found");
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        v2.setBackgroundColor(getResources().getColor(R.color.white));
        ongoingOrders.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        deliveredOrders.setTextColor(getResources().getColor(R.color.black));
        if (gpsTracker.canGetLocation()) {

            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());

            hit = true;

            if (ConnectivityReceiver.isConnected())
                DELIVERYBOYORDERSLIST();
            else
                util.IOSDialog(context, util.internet_Connection_Error);
        } else {
            EnableGPSAutoMatically();
        }

    }

    private void DELIVERYBOYORDERSLIST() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        try {
            if (mDialog != null)
                mDialog.show();
        } catch (Exception e) {
        }
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.DELIVERYBOYORDERSLIST, formBody) {
            @Override
            public void getValueParse(String result) {
                upcoming_list = new ArrayList<>();
                completed_list = new ArrayList<>();

                if (upcoming_list.size() > 0)
                    upcoming_list.size();

                if (completed_list.size() > 0)
                    completed_list.clear();

                EDIT_PROFILE(latitude, longitude, mDialog);
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject data = jsonmainObject.getJSONObject("body");
                            JSONArray upcoming = data.getJSONArray("upcoming");
                            JSONArray completed = data.getJSONArray("completed");
                            for (int i = 0; i < upcoming.length(); i++) {
                                JSONObject object = upcoming.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();

                                requestModel.setId(object.getString("id"));
                                requestModel.setCreated(object.optString("created"));
                                requestModel.setImage(object.getJSONObject("restaurant").optString("image"));
                                requestModel.setPrice(object.getString("price"));

                                requestModel.setInitialcost(object.optString(Parameters.INITIALCOST));
                                requestModel.setCommission(object.optString(Parameters.COMMSIION));
                                requestModel.setDeliverycharges(object.optString(Parameters.DELIVERYCHARGES));


                                requestModel.setUsername(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setStatus(object.getString("status"));
                                requestModel.setLocation(object.getString("location"));
                                requestModel.setLatitude(object.getString("latitude"));
                                requestModel.setLongitude(object.getString("longitude"));

                                requestModel.setUser_name(object.getJSONObject("user").getString("name"));
                                requestModel.setUser_image(object.getJSONObject("user").getString("image"));
                                requestModel.setRestaurant_name(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setRestaurant_image(object.getJSONObject("restaurant").optString("image"));
                                requestModel.setRestaurant_address(object.getJSONObject("restaurant").getString("address"));
                                requestModel.setRestaurant_lat(object.getJSONObject("restaurant").getString("latitude"));
                                requestModel.setRestaurant_lng(object.getJSONObject("restaurant").getString("longitude"));
                                //requestModel.setIsRated(object.getString("isRated"));
                                requestModel.setSpecialRequest(object.getString("specialRequest"));

                                requestModel.setPreparationTime(object.getString("preparationTime"));
                                requestModel.setReason(object.getString("reason"));
                                requestModel.setDeliveryBy(object.getString("deliveryBy"));//1=restorent ,2=deliev boy

                                requestModel.setEstimatedDeliveryTime(object.getString("estimatedDeliveryTime"));
                                requestModel.setExpectedTimeDelivery(object.getString("expectedTimeDelivery"));


                               /* requestModel.setAddress_name(object.getJSONObject("json").getJSONObject("address").getString("name"));
                                requestModel.setAddress_phone(object.getJSONObject("json").getJSONObject("address").getString("phone"));
                                requestModel.setAddress_houseNumber(object.getJSONObject("json").getJSONObject("address").getString("houseNumber"));
                                requestModel.setAddress_streetAddress(object.getJSONObject("json").getJSONObject("address").getString("streetAddress"));
                                requestModel.setAddress_city(object.getJSONObject("json").getJSONObject("address").getString("city"));
                                requestModel.setAddress_country(object.getJSONObject("json").getJSONObject("address").getString("country"));
                                requestModel.setAddress_zipCode(object.getJSONObject("json").getJSONObject("address").getString("zipCode"));*/

                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < object.getJSONObject("json").getJSONArray("foodMenuItems").length(); j++) {
                                    JSONObject obj = object.getJSONObject("json").getJSONArray("foodMenuItems").getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                requestModel.setList(list_food);
                                upcoming_list.add(requestModel);
                            }
                            for (int i = 0; i < completed.length(); i++) {
                                JSONObject object = completed.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setId(object.getString("id"));
                                requestModel.setCreated(object.optString("created"));
                                requestModel.setImage(object.getJSONObject("restaurant").optString("image"));
                                requestModel.setPrice(object.getString("price"));

                                requestModel.setInitialcost(object.optString(Parameters.INITIALCOST));
                                requestModel.setCommission(object.optString(Parameters.COMMSIION));
                                requestModel.setDeliverycharges(object.optString(Parameters.DELIVERYCHARGES));


                                requestModel.setUsername(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setStatus(object.getString("status"));
                                requestModel.setLocation(object.getString("location"));
                                requestModel.setLatitude(object.getString("latitude"));
                                requestModel.setLongitude(object.getString("longitude"));

                                requestModel.setUser_name(object.getJSONObject("user").getString("name"));
                                requestModel.setUser_image(object.getJSONObject("user").getString("image"));
                                requestModel.setRestaurant_name(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setRestaurant_image(object.getJSONObject("restaurant").getString("image"));
                                requestModel.setRestaurant_address(object.getJSONObject("restaurant").optString("address"));
                                requestModel.setRestaurant_lat(object.getJSONObject("restaurant").getString("latitude"));
                                requestModel.setRestaurant_lng(object.getJSONObject("restaurant").getString("longitude"));
                                // requestModel.setIsRated(object.getString("isRated"));
                                requestModel.setSpecialRequest(object.getString("specialRequest"));

                                requestModel.setPreparationTime(object.getString("preparationTime"));
                                requestModel.setReason(object.getString("reason"));
                                requestModel.setDeliveryBy(object.getString("deliveryBy"));//1=restorent ,2=deliev boy

                                requestModel.setEstimatedDeliveryTime(object.getString("estimatedDeliveryTime"));
                                requestModel.setExpectedTimeDelivery(object.getString("expectedTimeDelivery"));


                               /* requestModel.setAddress_name(object.getJSONObject("json").getJSONObject("address").getString("name"));
                                requestModel.setAddress_phone(object.getJSONObject("json").getJSONObject("address").getString("phone"));
                                requestModel.setAddress_houseNumber(object.getJSONObject("json").getJSONObject("address").getString("houseNumber"));
                                requestModel.setAddress_streetAddress(object.getJSONObject("json").getJSONObject("address").getString("streetAddress"));
                                requestModel.setAddress_city(object.getJSONObject("json").getJSONObject("address").getString("city"));
                                requestModel.setAddress_country(object.getJSONObject("json").getJSONObject("address").getString("country"));
                                requestModel.setAddress_zipCode(object.getJSONObject("json").getJSONObject("address").getString("zipCode"));*/

                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < object.getJSONObject("json").getJSONArray("foodMenuItems").length(); j++) {
                                    JSONObject obj = object.getJSONObject("json").getJSONArray("foodMenuItems").getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                requestModel.setList(list_food);
                                completed_list.add(requestModel);
                            }

                         /*   if (upcoming_list.size() > 0)
                                Collections.reverse(upcoming_list);

                            if (completed_list.size() > 0)
                                Collections.reverse(completed_list);*/


                            if (upcoming_list.size() > 0) {

                                adapter = new DeliveryExpertOngoingOrdersAdapter(context, upcoming_list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("No Order Found");
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void EDIT_PROFILE(String latitude, String longitude, ProgressDialog mDialog) {
        Log.e("latlong", latitude + " " + longitude);
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
        formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                try {
                    if (mDialog != null)
                        mDialog.dismiss();
                } catch (Exception e) {
                }
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setAuthorization_key(body.getString("token"));
                            //Toast.makeText(context, "lat long Successfully!", Toast.LENGTH_SHORT).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext());
                            if (checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    Activity#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for Activity#requestPermissions for more details.
                                return;
                            }
                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());


                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }


                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                location = locationList.get(locationList.size() - 1);
                // util.showToast(getActivity(), "lat long " + location.getLatitude());


                /*gpsTracker = new GPSTracker(context);
                //util.showToast(context, "latt " + String.valueOf(gpsTracker.getLatitude()));
                if (ConnectivityReceiver.isConnected()) {
                    AGENCIE("", true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }*/

                if (latitude.isEmpty() || latitude == null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLatitude());

                }


                if (ConnectivityReceiver.isConnected()) {
                    if (!hit)
                        DELIVERYBOYORDERSLIST();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }


                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

