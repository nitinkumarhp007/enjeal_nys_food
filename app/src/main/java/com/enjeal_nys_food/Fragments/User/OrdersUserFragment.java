package com.enjeal_nys_food.Fragments.User;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.enjeal_nys_food.Activities.SignInActivity;
import com.enjeal_nys_food.Adapters.NewOrderAdapter;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncGet;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.google.zxing.WriterException;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import androidmads.library.qrgenearator.QRGSaver;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OrdersUserFragment extends Fragment {

    Context context;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;
    @BindView(R.id.requests)
    Button requests;
    @BindView(R.id.ongoing)
    Button ongoing;
    @BindView(R.id.past)
    Button past;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.v3)
    View v3;

    NewOrderAdapter adapter;

    ArrayList<RequestModel> list;
    ArrayList<RequestModel> upcoming_list;
    ArrayList<RequestModel> completed_list;

    public OrdersUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_orders_user, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        v2.setBackgroundColor(getResources().getColor(R.color.white));
        v3.setBackgroundColor(getResources().getColor(R.color.white));
        ongoing.setTextColor(getResources().getColor(R.color.black));
        past.setTextColor(getResources().getColor(R.color.black));
        requests.setTextColor(getResources().getColor(R.color.colorPrimaryDark));


        if (ConnectivityReceiver.isConnected()) {
            REQUESTFOODLIST_USER();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        return view;
    }

    private void REQUESTFOODLIST_USER() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FOODCATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.REQUESTFOODLIST_USER, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setId(object.getString("id"));
                                requestModel.setCreated(object.optString("created"));
                                requestModel.setImage(object.getJSONObject("restaurant").getString("image"));
                                requestModel.setPrice(object.getString("price"));

                                requestModel.setInitialcost(object.optString(Parameters.INITIALCOST));
                                requestModel.setCommission(object.optString(Parameters.COMMSIION));
                                requestModel.setDeliverycharges(object.optString(Parameters.DELIVERYCHARGES));


                                requestModel.setUsername(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setStatus(object.getString("status"));

                                Log.e("status",object.getString("status"));

                                requestModel.setLocation(object.getString("location"));
                                requestModel.setLatitude(object.getString("latitude"));
                                requestModel.setLongitude(object.getString("longitude"));

                                requestModel.setPreparationTime(object.getString("preparationTime"));
                                requestModel.setReason(object.getString("reason"));
                                requestModel.setDeliveryBy(object.getString("deliveryBy"));//1=restorent ,2=deliev boy

                                requestModel.setEstimatedDeliveryTime(object.getString("estimatedDeliveryTime"));
                                requestModel.setExpectedTimeDelivery(object.getString("expectedTimeDelivery"));

                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < object.getJSONObject("json").getJSONArray("foodMenuItems").length(); j++) {
                                    JSONObject obj = object.getJSONObject("json").getJSONArray("foodMenuItems").getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                requestModel.setList(list_food);
                                list.add(requestModel);
                            }
                            if (list.size() > 0) {
                                adapter = new NewOrderAdapter(context, list, OrdersUserFragment.this, false);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("No Request Found");
                            }


                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.requests, R.id.ongoing, R.id.past})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.requests:
                if (ConnectivityReceiver.isConnected()) {
                    v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    v2.setBackgroundColor(getResources().getColor(R.color.white));
                    v3.setBackgroundColor(getResources().getColor(R.color.white));
                    ongoing.setTextColor(getResources().getColor(R.color.black));
                    past.setTextColor(getResources().getColor(R.color.black));
                    requests.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    myRecyclerView.setVisibility(View.GONE);

                    REQUESTFOODLIST_USER();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }

                break;
            case R.id.ongoing:

                if (ConnectivityReceiver.isConnected()) {
                    v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    v1.setBackgroundColor(getResources().getColor(R.color.white));
                    v3.setBackgroundColor(getResources().getColor(R.color.white));
                    ongoing.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    past.setTextColor(getResources().getColor(R.color.black));
                    requests.setTextColor(getResources().getColor(R.color.black));
                    myRecyclerView.setVisibility(View.GONE);

                    USERORDERLIST("1");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }

                break;
            case R.id.past:
                if (ConnectivityReceiver.isConnected()) {
                    v3.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    v1.setBackgroundColor(getResources().getColor(R.color.white));
                    v2.setBackgroundColor(getResources().getColor(R.color.white));
                    ongoing.setTextColor(getResources().getColor(R.color.black));
                    past.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    requests.setTextColor(getResources().getColor(R.color.black));

                    USERORDERLIST("2");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    public void ORDER_PAYMENT(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ORDERID, list.get(position).getId());
        formBuilder.addFormDataPart(Parameters.PAYMENTMETHOD, "0");//0=cash , 1=paypal , 2= stripe
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("Order Placed Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (ConnectivityReceiver.isConnected()) {
                                        myRecyclerView.setVisibility(View.GONE);
                                        v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                        v1.setBackgroundColor(getResources().getColor(R.color.white));
                                        v3.setBackgroundColor(getResources().getColor(R.color.white));
                                        ongoing.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                        past.setTextColor(getResources().getColor(R.color.black));
                                        requests.setTextColor(getResources().getColor(R.color.black));
                                        USERORDERLIST("1");
                                    } else {
                                        util.IOSDialog(context, util.internet_Connection_Error);
                                    }
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("message"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void USERORDERLIST(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.USERORDERLIST, formBody) {
            @Override
            public void getValueParse(String result) {
                upcoming_list = new ArrayList<>();
                completed_list = new ArrayList<>();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject data = jsonmainObject.getJSONObject("body");
                            JSONArray upcoming = data.getJSONArray("upcoming");
                            JSONArray completed = data.getJSONArray("completed");
                            for (int i = 0; i < upcoming.length(); i++) {
                                JSONObject object = upcoming.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setId(object.getString("id"));
                                requestModel.setCreated(object.optString("created"));
                                requestModel.setImage(object.getJSONObject("restaurant").optString("image"));

                                requestModel.setInitialcost(object.optString(Parameters.INITIALCOST));
                                requestModel.setCommission(object.optString(Parameters.COMMSIION));
                                requestModel.setDeliverycharges(object.optString(Parameters.DELIVERYCHARGES));

                                requestModel.setPrice(object.getString("price"));
                                requestModel.setUsername(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setStatus(object.getString("status"));

                                requestModel.setUser_id(object.getJSONObject("user").getString("id"));
                                requestModel.setUser_name(object.getJSONObject("user").getString("name"));
                                requestModel.setUser_image(object.getJSONObject("user").getString("image"));
                                requestModel.setRestaurant_id(object.getJSONObject("restaurant").getString("id"));
                                requestModel.setRestaurant_name(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setRestaurant_image(object.getJSONObject("restaurant").getString("image"));

                                if (!object.optString("deliveryExpert").equals("null")) {
                                    requestModel.setName_d(object.optJSONObject("deliveryExpert").optString("name"));
                                    requestModel.setPhone_d(object.optJSONObject("deliveryExpert").optString("phone"));
                                    requestModel.setImage_d(object.optJSONObject("deliveryExpert").optString("image"));
                                }


                                requestModel.setIsRated(object.getString("isRated"));
                                requestModel.setSpecialRequest(object.getString("specialRequest"));
                                requestModel.setLocation(object.getString("location"));
                                requestModel.setLatitude(object.getString("latitude"));
                                requestModel.setLongitude(object.getString("longitude"));
                                requestModel.setEstimatedDeliveryTime(object.getString("estimatedDeliveryTime"));
                                requestModel.setExpectedTimeDelivery(object.getString("expectedTimeDelivery"));

                                requestModel.setQr_code_image(object.getString("qrImage"));
                                requestModel.setPreparationTime(object.getString("preparationTime"));
                                requestModel.setReason(object.getString("reason"));
                                requestModel.setDeliveryBy(object.getString("deliveryBy"));//1=restorent ,2=deliev boy


                              /*  requestModel.setAddress_name(object.getJSONObject("json").getJSONObject("address").getString("name"));
                                requestModel.setAddress_phone(object.getJSONObject("json").getJSONObject("address").getString("phone"));
                                requestModel.setAddress_houseNumber(object.getJSONObject("json").getJSONObject("address").getString("houseNumber"));
                                requestModel.setAddress_streetAddress(object.getJSONObject("json").getJSONObject("address").getString("streetAddress"));
                                requestModel.setAddress_city(object.getJSONObject("json").getJSONObject("address").getString("city"));
                                requestModel.setAddress_country(object.getJSONObject("json").getJSONObject("address").getString("country"));
                                requestModel.setAddress_zipCode(object.getJSONObject("json").getJSONObject("address").getString("zipCode"));*/

                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < object.getJSONObject("json").getJSONArray("foodMenuItems").length(); j++) {
                                    JSONObject obj = object.getJSONObject("json").getJSONArray("foodMenuItems").getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                requestModel.setList(list_food);
                                upcoming_list.add(requestModel);
                            }
                            for (int i = 0; i < completed.length(); i++) {
                                JSONObject object = completed.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setId(object.getString("id"));
                                requestModel.setCreated(object.optString("created"));
                                requestModel.setImage(object.getJSONObject("restaurant").optString("image"));

                                requestModel.setInitialcost(object.optString(Parameters.INITIALCOST));
                                requestModel.setCommission(object.optString(Parameters.COMMSIION));
                                requestModel.setDeliverycharges(object.optString(Parameters.DELIVERYCHARGES));


                                requestModel.setPrice(object.getString("price"));
                                requestModel.setUsername(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setStatus(object.getString("status"));

                                requestModel.setUser_id(object.getJSONObject("user").getString("id"));
                                requestModel.setUser_name(object.getJSONObject("user").getString("name"));
                                requestModel.setUser_image(object.getJSONObject("user").getString("image"));
                                requestModel.setRestaurant_id(object.getJSONObject("restaurant").getString("id"));
                                requestModel.setRestaurant_name(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setRestaurant_image(object.getJSONObject("restaurant").getString("image"));
                                requestModel.setIsRated(object.getString("isRated"));
                                requestModel.setSpecialRequest(object.getString("specialRequest"));
                                requestModel.setLocation(object.getString("location"));
                                requestModel.setLatitude(object.getString("latitude"));
                                requestModel.setLongitude(object.getString("longitude"));
                                requestModel.setEstimatedDeliveryTime(object.getString("estimatedDeliveryTime"));
                                requestModel.setExpectedTimeDelivery(object.getString("expectedTimeDelivery"));

                                if (!object.optString("deliveryExpert").equals("null")) {
                                    requestModel.setName_d(object.optJSONObject("deliveryExpert").optString("name"));
                                    requestModel.setPhone_d(object.optJSONObject("deliveryExpert").optString("phone"));
                                    requestModel.setImage_d(object.optJSONObject("deliveryExpert").optString("image"));
                                }


                                requestModel.setQr_code_image(object.getString("qrImage"));
                                requestModel.setPreparationTime(object.getString("preparationTime"));
                                requestModel.setReason(object.getString("reason"));
                                requestModel.setDeliveryBy(object.getString("deliveryBy"));//1=restorent ,2=deliev boy


                                /*requestModel.setAddress_name(object.getJSONObject("json").getJSONObject("address").getString("name"));
                                requestModel.setAddress_phone(object.getJSONObject("json").getJSONObject("address").getString("phone"));
                                requestModel.setAddress_houseNumber(object.getJSONObject("json").getJSONObject("address").getString("houseNumber"));
                                requestModel.setAddress_streetAddress(object.getJSONObject("json").getJSONObject("address").getString("streetAddress"));
                                requestModel.setAddress_city(object.getJSONObject("json").getJSONObject("address").getString("city"));
                                requestModel.setAddress_country(object.getJSONObject("json").getJSONObject("address").getString("country"));
                                requestModel.setAddress_zipCode(object.getJSONObject("json").getJSONObject("address").getString("zipCode"));*/

                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < object.getJSONObject("json").getJSONArray("foodMenuItems").length(); j++) {
                                    JSONObject obj = object.getJSONObject("json").getJSONArray("foodMenuItems").getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                requestModel.setList(list_food);
                                completed_list.add(requestModel);
                            }

                            if (type.equals("1")) {
                                if (upcoming_list.size() > 0) {
                                    adapter = new NewOrderAdapter(context, upcoming_list, OrdersUserFragment.this, true);
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myRecyclerView.setAdapter(adapter);

                                    myRecyclerView.setVisibility(View.VISIBLE);
                                    errorMessage.setVisibility(View.GONE);
                                } else {
                                    myRecyclerView.setVisibility(View.GONE);
                                    errorMessage.setVisibility(View.VISIBLE);
                                    errorMessage.setText("No Order Found");
                                }
                            } else {
                                if (completed_list.size() > 0) {
                                    adapter = new NewOrderAdapter(context, completed_list, OrdersUserFragment.this, true);
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myRecyclerView.setAdapter(adapter);
                                    myRecyclerView.setVisibility(View.VISIBLE);
                                    errorMessage.setVisibility(View.GONE);
                                } else {
                                    myRecyclerView.setVisibility(View.GONE);
                                    errorMessage.setVisibility(View.VISIBLE);
                                    errorMessage.setText("No Order Found");
                                }
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}
