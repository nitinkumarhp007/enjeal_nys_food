package com.enjeal_nys_food.Fragments.User.DeliveryBoy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Activities.ChangePasswordActivity;
import com.enjeal_nys_food.Activities.SignInActivity;
import com.enjeal_nys_food.Activities.TermConditionActivity;
import com.enjeal_nys_food.Activities.UpdateProfileActivity;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.GPSTracker;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;


public class ProfileDeliveryFragment extends Fragment {

    Context context;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.rating_bar_layout)
    RelativeLayout ratingBarLayout;
    @BindView(R.id.edit_profile_text)
    TextView editProfileText;
    @BindView(R.id.edit_profile111)
    RelativeLayout editProfile111;
    @BindView(R.id.change_password)
    TextView changePassword;
    @BindView(R.id.change_password___)
    RelativeLayout changePassword_;
    @BindView(R.id.change_language)
    TextView change_language;
    @BindView(R.id.change_language___)
    RelativeLayout change_language___;
    @BindView(R.id.about_us)
    TextView aboutUs;
    @BindView(R.id.about_us_layout)
    RelativeLayout aboutUsLayout;
    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.terms_layout)
    RelativeLayout termsLayout;
    @BindView(R.id.logout)
    TextView logout;
    @BindView(R.id.delete_account)
    TextView deleteAccount;
    @BindView(R.id.delete_account___)
    RelativeLayout deleteAccount___;
    @BindView(R.id.logout_layout)
    RelativeLayout logoutLayout;
    @BindView(R.id.location)
    TextView location;
    private SavePref savePref;

    GPSTracker gpsTracker;

    Unbinder unbinder;

    public ProfileDeliveryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delivery_profile_user, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        gpsTracker=new GPSTracker(context);
        savePref = new SavePref(context);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        name.setText(savePref.getName());
        email.setText(savePref.getEmail());
        phone.setText(savePref.getStringLatest(Parameters.COUNTRY_CODE) + savePref.getPhone());
        location.setText(savePref.getStringLatest(Parameters.ADDRESS));
        Glide.with(context).load(savePref.getImage()).error(R.drawable.placeholder).into(profilePic);

        if (!gpsTracker.canGetLocation()) {
            gpsTracker.showSettingsAlert();
        } else {
            location.setText(util.getCompleteAddressString(context, gpsTracker.getLatitude(), gpsTracker.getLongitude()));

           /* latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
*/

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Log.e("dataaa","address_ "+address_);
                Log.e("dataaa","city "+city);
                Log.e("dataaa","state "+state);
                Log.e("dataaa","country "+country);
                Log.e("dataaa","postalCode "+postalCode);
                Log.e("dataaa","knownName "+knownName);

                Log.e("dataaa","subadminaera "+addresses.get(0).getSubAdminArea());
                Log.e("dataaa","subalocality "+addresses.get(0).getSubLocality());

                location.setText(address_);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.edit_profile_text, R.id.edit_profile111, R.id.delete_account, R.id.delete_account___, R.id.change_language, R.id.change_language___, R.id.change_password, R.id.change_password___, R.id.about_us, R.id.about_us_layout, R.id.terms, R.id.terms_layout, R.id.logout, R.id.logout_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit_profile_text:
                Intent intent = new Intent(getContext(), UpdateProfileActivity.class);
                intent.putExtra("is_from_delivery", true);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile111:
                Intent intent111 = new Intent(getContext(), UpdateProfileActivity.class);
                intent111.putExtra("is_from_delivery", true);
                startActivity(intent111);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password:
                startActivity(new Intent(context, ChangePasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password___:
                startActivity(new Intent(context, ChangePasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_language:
                LanguageDialog();
                break;
            case R.id.change_language___:
                LanguageDialog();
                break;
            case R.id.about_us:
                TermPageTask("privacy");
                break;
            case R.id.about_us_layout:
                TermPageTask("privacy");
                break;
            case R.id.terms:
                TermPageTask("term");
                break;
            case R.id.terms_layout:
                TermPageTask("term");
                break;
            case R.id.delete_account:
                DeleteAlert();
                break;
            case R.id.delete_account___:
                DeleteAlert();
                break;
            case R.id.logout:
                LogoutAlert();
                break;
            case R.id.logout_layout:
                LogoutAlert();
                break;
        }
    }
    private void TermPageTask(String type)
    {
        Intent intent = new Intent(context, TermConditionActivity.class);
        intent.putExtra("type", type);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
    private void DeleteAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Delete your account?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        util.showToast(context, "Delete Successfully!");
                        Intent intent = new Intent(context, SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void LOGOUT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.COUNTRY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.clearPreferences();
                            util.showToast(context, "Logout Successfully!");
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            savePref.clearPreferences();
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void LanguageDialog() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Language");

        /*ArrayList to Array Conversion */
        String array[] = new String[2];
        array[0] = "English";
        array[1] = "French";


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

               /* if(which==0)
                {
                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                else if(which==1) {
                    startActivity(new Intent(SignInActivity.this, RestaurtantMainActivity.class));
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                */
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void LogoutAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LOGOUT_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }


}
