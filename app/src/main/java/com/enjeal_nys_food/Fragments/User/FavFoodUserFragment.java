package com.enjeal_nys_food.Fragments.User;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enjeal_nys_food.Adapters.FavUserAdapter;
import com.enjeal_nys_food.ModelClasses.UserModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncGet;
import com.enjeal_nys_food.parser.GetAsyncPut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class FavFoodUserFragment extends Fragment {


    Context context;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;
    FavUserAdapter favUserAdapter;
    private ArrayList<UserModel> list;

    public FavFoodUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fav_food_user, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            FAV_RESTAURANTLISTING();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void FAV_RESTAURANTLISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.FAV_RESTAURANT, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                UserModel userModel = new UserModel();
                                userModel.setId(object.getString("id"));
                                userModel.setEmail(object.optString("email"));
                                userModel.setImage(object.optString("image"));
                                userModel.setName(object.optString("name"));
                                userModel.setDistance(object.optString("distance"));
                                userModel.setIs_fav(object.optString("isFav"));
                                userModel.setAvgRating(object.optString("avgRating"));
                                list.add(userModel);
                            }
                            if (list.size() == 0) {
                                myRecyclerView.setVisibility(View.INVISIBLE);
                                errorMessage.setVisibility(View.VISIBLE);
                            } else {
                                errorMessage.setVisibility(View.INVISIBLE);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                favUserAdapter = new FavUserAdapter(context, list, FavFoodUserFragment.this);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                                myRecyclerView.setAdapter(favUserAdapter);
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void FAV_UNFAV_RESTAURANT(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.RESTAURANTID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.FAV_UNFAV_RESTAURANT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, "Restaurant Unfavorite Successfully!");
                            list.remove(position);

                            if (list.size() == 0) {
                                myRecyclerView.setVisibility(View.INVISIBLE);
                                errorMessage.setVisibility(View.VISIBLE);
                            } else {
                                errorMessage.setVisibility(View.INVISIBLE);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                favUserAdapter.notifyDataSetChanged();
                            }


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
