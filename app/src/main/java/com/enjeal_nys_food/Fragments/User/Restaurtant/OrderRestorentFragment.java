package com.enjeal_nys_food.Fragments.User.Restaurtant;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.enjeal_nys_food.Adapters.NewOrdersRestorentAdapter;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OrderRestorentFragment extends Fragment {

    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;

    Context context;
    @BindView(R.id.ongoing_orders)
    Button ongoingOrders;
    @BindView(R.id.completed)
    Button completed;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.error_message)
    TextView errorMessage;

    ArrayList<RequestModel> upcoming_list;
    ArrayList<RequestModel> completed_list;

    public OrderRestorentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_restorent, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();


        if (ConnectivityReceiver.isConnected()) {
            RESTAURANTORDERLIST("1");
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        return view;
    }

    private void RESTAURANTORDERLIST(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.RESTAURANTORDERLIST, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                upcoming_list = new ArrayList<>();
                completed_list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject data = jsonmainObject.getJSONObject("body");
                            JSONArray upcoming = data.getJSONArray("upcoming");
                            JSONArray completed = data.getJSONArray("completed");
                            for (int i = 0; i < upcoming.length(); i++) {
                                JSONObject object = upcoming.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setId(object.getString("id"));
                                requestModel.setCreated(object.getString("created"));
                                requestModel.setImage(object.getJSONObject("user").optString("image"));
                                requestModel.setPrice(object.getString("price"));

                                requestModel.setInitialcost(object.optString(Parameters.INITIALCOST));
                                requestModel.setCommission(object.optString(Parameters.COMMSIION));
                                requestModel.setDeliverycharges(object.optString(Parameters.DELIVERYCHARGES));

                                requestModel.setUsername(object.getJSONObject("user").getString("name"));
                                requestModel.setStatus(object.getString("status"));

                                requestModel.setUser_id(object.getJSONObject("user").getString("id"));
                                requestModel.setUser_name(object.getJSONObject("user").getString("name"));
                                requestModel.setUser_image(object.getJSONObject("user").getString("image"));
                                requestModel.setRestaurant_id(object.getJSONObject("restaurant").getString("id"));
                                requestModel.setRestaurant_name(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setRestaurant_image(object.getJSONObject("restaurant").getString("image"));
                                requestModel.setIsRated(object.getString("isRated"));
                                requestModel.setSpecialRequest(object.getString("specialRequest"));

                                requestModel.setLocation(object.getString("location"));
                                requestModel.setLatitude(object.getString("latitude"));
                                requestModel.setLongitude(object.getString("longitude"));
                                requestModel.setEstimatedDeliveryTime(object.getString("estimatedDeliveryTime"));
                                requestModel.setExpectedTimeDelivery(object.getString("expectedTimeDelivery"));

                                if (!object.optString("deliveryExpert").equals("null")) {
                                    requestModel.setName_d(object.optJSONObject("deliveryExpert").optString("name"));
                                    requestModel.setPhone_d(object.optJSONObject("deliveryExpert").optString("phone"));
                                    requestModel.setImage_d(object.optJSONObject("deliveryExpert").optString("image"));
                                }


                              /*  if (!object.optString("deliveryExpert").equals("null")) {
                                    requestModel.setName_d(object.optJSONObject("deliveryExpert").optString("name"));
                                    requestModel.setPhone_d(object.optJSONObject("deliveryExpert").optString("phone"));
                                    requestModel.setImage_d(object.optJSONObject("deliveryExpert").optString("image"));
                                }*/


                                requestModel.setQr_code_image(object.getString("qrImage"));
                                requestModel.setPreparationTime(object.getString("preparationTime"));
                                requestModel.setReason(object.getString("reason"));
                                requestModel.setDeliveryBy(object.getString("deliveryBy"));//1=restorent ,2=deliev boy

                                /*requestModel.setAddress_name(object.getJSONObject("json").getJSONObject("address").getString("name"));
                                requestModel.setAddress_phone(object.getJSONObject("json").getJSONObject("address").getString("phone"));
                                requestModel.setAddress_houseNumber(object.getJSONObject("json").getJSONObject("address").getString("houseNumber"));
                                requestModel.setAddress_streetAddress(object.getJSONObject("json").getJSONObject("address").getString("streetAddress"));
                                requestModel.setAddress_city(object.getJSONObject("json").getJSONObject("address").getString("city"));
                                requestModel.setAddress_country(object.getJSONObject("json").getJSONObject("address").getString("country"));
                                requestModel.setAddress_zipCode(object.getJSONObject("json").getJSONObject("address").getString("zipCode"));*/


                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < object.getJSONObject("json").getJSONArray("foodMenuItems").length(); j++) {
                                    JSONObject obj = object.getJSONObject("json").getJSONArray("foodMenuItems").getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                requestModel.setList(list_food);
                                upcoming_list.add(requestModel);
                            }
                            for (int i = 0; i < completed.length(); i++) {
                                JSONObject object = completed.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setId(object.getString("id"));
                                requestModel.setCreated(object.getString("created"));
                                requestModel.setImage(object.getJSONObject("user").optString("image"));
                                requestModel.setPrice(object.getString("price"));

                                requestModel.setInitialcost(object.optString(Parameters.INITIALCOST));
                                requestModel.setCommission(object.optString(Parameters.COMMSIION));
                                requestModel.setDeliverycharges(object.optString(Parameters.DELIVERYCHARGES));

                                requestModel.setUsername(object.getJSONObject("user").getString("name"));
                                requestModel.setStatus(object.getString("status"));

                                requestModel.setUser_id(object.getJSONObject("user").getString("id"));
                                requestModel.setUser_name(object.getJSONObject("user").getString("name"));
                                requestModel.setUser_image(object.getJSONObject("user").getString("image"));
                                requestModel.setRestaurant_id(object.getJSONObject("restaurant").getString("id"));
                                requestModel.setRestaurant_name(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setRestaurant_image(object.getJSONObject("restaurant").getString("image"));
                                requestModel.setIsRated(object.getString("isRated"));
                                requestModel.setSpecialRequest(object.getString("specialRequest"));

                                requestModel.setLocation(object.getString("location"));
                                requestModel.setLatitude(object.getString("latitude"));
                                requestModel.setLongitude(object.getString("longitude"));
                                requestModel.setEstimatedDeliveryTime(object.getString("estimatedDeliveryTime"));
                                requestModel.setExpectedTimeDelivery(object.getString("expectedTimeDelivery"));


                               /* if (!object.optString("deliveryExpert").equals("null")) {
                                    requestModel.setName_d(object.optJSONObject("deliveryExpert").optString("name"));
                                    requestModel.setPhone_d(object.optJSONObject("deliveryExpert").optString("phone"));
                                    requestModel.setImage_d(object.optJSONObject("deliveryExpert").optString("image"));
                                }*/

                                requestModel.setQr_code_image(object.getString("qrImage"));
                                requestModel.setPreparationTime(object.getString("preparationTime"));
                                requestModel.setReason(object.getString("reason"));
                                requestModel.setDeliveryBy(object.getString("deliveryBy"));//1=restorent ,2=deliev boy



                                if (!object.optString("deliveryExpert").equals("null")) {
                                    requestModel.setName_d(object.optJSONObject("deliveryExpert").optString("name"));
                                    requestModel.setPhone_d(object.optJSONObject("deliveryExpert").optString("phone"));
                                    requestModel.setImage_d(object.optJSONObject("deliveryExpert").optString("image"));
                                }

                              /*  requestModel.setAddress_name(object.getJSONObject("json").getJSONObject("address").getString("name"));
                                requestModel.setAddress_phone(object.getJSONObject("json").getJSONObject("address").getString("phone"));
                                requestModel.setAddress_houseNumber(object.getJSONObject("json").getJSONObject("address").getString("houseNumber"));
                                requestModel.setAddress_streetAddress(object.getJSONObject("json").getJSONObject("address").getString("streetAddress"));
                                requestModel.setAddress_city(object.getJSONObject("json").getJSONObject("address").getString("city"));
                                requestModel.setAddress_country(object.getJSONObject("json").getJSONObject("address").getString("country"));
                                requestModel.setAddress_zipCode(object.getJSONObject("json").getJSONObject("address").getString("zipCode"));*/


                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < object.getJSONObject("json").getJSONArray("foodMenuItems").length(); j++) {
                                    JSONObject obj = object.getJSONObject("json").getJSONArray("foodMenuItems").getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                requestModel.setList(list_food);
                                completed_list.add(requestModel);
                            }
                            if (type.equals("1")) {
                                if (upcoming_list.size() > 0) {
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myRecyclerView.setAdapter(new NewOrdersRestorentAdapter(context, upcoming_list));
                                    myRecyclerView.setVisibility(View.VISIBLE);
                                    errorMessage.setVisibility(View.GONE);
                                } else {
                                    myRecyclerView.setVisibility(View.GONE);
                                    errorMessage.setVisibility(View.VISIBLE);
                                    errorMessage.setText("No Order Found");
                                }
                            } else {
                                if (completed_list.size() > 0) {
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myRecyclerView.setAdapter(new NewOrdersRestorentAdapter(context, completed_list));
                                    myRecyclerView.setVisibility(View.VISIBLE);
                                    errorMessage.setVisibility(View.GONE);
                                } else {
                                    myRecyclerView.setVisibility(View.GONE);
                                    errorMessage.setVisibility(View.VISIBLE);
                                    errorMessage.setText("No Order Found");
                                }
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ongoing_orders, R.id.completed})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ongoing_orders:
                if (ConnectivityReceiver.isConnected()) {
                    ongoingOrders.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                    completed.setTextColor(context.getResources().getColor(R.color.black));
                    v1.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
                    v2.setBackgroundColor(context.getResources().getColor(R.color.white));
                    RESTAURANTORDERLIST("1");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }

                break;
            case R.id.completed:
                if (ConnectivityReceiver.isConnected()) {
                    completed.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                    ongoingOrders.setTextColor(context.getResources().getColor(R.color.black));
                    v2.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
                    v1.setBackgroundColor(context.getResources().getColor(R.color.white));
                    RESTAURANTORDERLIST("2");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }
}
