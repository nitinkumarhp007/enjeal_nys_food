package com.enjeal_nys_food.Fragments.User.Restaurtant;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.enjeal_nys_food.Activities.AddFoodActivity;
import com.enjeal_nys_food.Adapters.PrimaryMenuAdapter;
import com.enjeal_nys_food.ModelClasses.CategoryModel;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncDELETE;
import com.enjeal_nys_food.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;


public class MenuRestorentFragment extends Fragment {


    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.add_button)
    Button addButton;
    Unbinder unbinder;
    Context context;
    @BindView(R.id.search_bar)
    EditText searchBar;
    private SavePref savePref;
    ArrayList<CategoryModel> list;
    @BindView(R.id.error_message)
    TextView errorMessage;
    PrimaryMenuAdapter adapter;

    public MenuRestorentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu_restorent, container, false);
        unbinder = ButterKnife.bind(this, view);


        context = getActivity();
        savePref = new SavePref(context);


        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            FOODDFOODMENUITEMLISTING();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void FOODDFOODMENUITEMLISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.FOODMENUITEMSLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.getString("image"));
                                JSONArray foodMenuItems = object.getJSONArray("foodMenuItems");
                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < foodMenuItems.length(); j++) {
                                    JSONObject obj = foodMenuItems.getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setId(obj.getString("id"));
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setDescription(obj.optString("description"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setCategory_name(object.optString("name"));
                                    menuItemModel.setQuantity(obj.optString("quantity"));
                                    menuItemModel.setCategory_id(obj.optString("categoryId"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                categoryModel.setList(list_food);
                                list.add(categoryModel);
                            }
                            if (list.size() > 0) {
                                adapter = new PrimaryMenuAdapter(context, true, list, MenuRestorentFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.INVISIBLE);
                            } else {
                                myRecyclerView.setVisibility(View.INVISIBLE);
                                errorMessage.setVisibility(View.VISIBLE);
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DeleteAlert(int position, ArrayList<MenuItemModel> list) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setMessage("Are you sure to Delete?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETEFOODMENUITEM(position, list);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETEFOODMENUITEM(int position, ArrayList<MenuItemModel> list_food) {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, list_food.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.DELETEFOODMENUITEM, formBody, mDialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response result) {
                mDialog.dismiss();
                try {
                    Log.e("resulttttt", result.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (result.isSuccessful()) {
                    new IOSDialog.Builder(context)
                            .setCancelable(false)
                            .setMessage("Food Item Deleted Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FOODDFOODMENUITEMLISTING();
                        }
                    }).show();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.add_button)
    public void onViewClicked() {
        Intent intent = new Intent(getContext(), AddFoodActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}
