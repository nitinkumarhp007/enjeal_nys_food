package com.enjeal_nys_food.Fragments.User.Restaurtant;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enjeal_nys_food.Activities.SignInActivity;
import com.enjeal_nys_food.Adapters.OrderRequestAdapter;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.enjeal_nys_food.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class HomeRestorentFragment extends Fragment {


    Context context;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;
    OrderRequestAdapter adapter;
    ArrayList<RequestModel> list;

    public HomeRestorentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_restorent, container, false);
        unbinder = ButterKnife.bind(this, view);


        context = getActivity();
        savePref = new SavePref(context);

        Log.e("token____", SavePref.getDeviceToken(context, "token"));


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected())
            REQUESTFOODLIST_RESTAURANT();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void REQUESTFOODLIST_RESTAURANT() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        try {
            if (mDialog != null)
                mDialog.show();
        } catch (Exception e) {
        }

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FOODCATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.REQUESTFOODLIST_RESTAURANT, formBody) {
            @Override
            public void getValueParse(String result) {

                try {
                    if (mDialog != null)
                        mDialog.dismiss();
                } catch (Exception e) {
                }
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setId(object.getString("id"));
                                requestModel.setCreated(object.optString("created"));
                                requestModel.setImage(object.getJSONObject("user").getString("image"));
                                requestModel.setPrice(object.getString("price"));

                                requestModel.setInitialcost(object.optString(Parameters.INITIALCOST));
                                requestModel.setCommission(object.optString(Parameters.COMMSIION));
                                requestModel.setDeliverycharges(object.optString(Parameters.DELIVERYCHARGES));

                                requestModel.setUsername(object.getJSONObject("user").getString("name"));
                                requestModel.setStatus(object.getString("status"));

                                Log.e("status",object.getString("status"));
                                requestModel.setLocation(object.getString("location"));
                                requestModel.setLatitude(object.getString("latitude"));
                                requestModel.setLongitude(object.getString("longitude"));
                                requestModel.setEstimatedDeliveryTime(object.getString("estimatedDeliveryTime"));
                                requestModel.setExpectedTimeDelivery(object.getString("expectedTimeDelivery"));

                                requestModel.setUser_name(object.getJSONObject("user").getString("name"));
                                requestModel.setUser_image(object.getJSONObject("user").getString("image"));
                                requestModel.setRestaurant_name(object.getJSONObject("restaurant").getString("name"));
                                requestModel.setRestaurant_image(object.getJSONObject("restaurant").getString("image"));
                                requestModel.setIsRated(object.optString("isRated"));
                                requestModel.setSpecialRequest(object.getString("specialityOnFoodOrder"));
                                requestModel.setAddress_name(object.getString("location"));
                                /*requestModel.setAddress_name(object.getJSONObject("json").getJSONObject("address").getString("name"));
                                requestModel.setAddress_phone(object.getJSONObject("json").getJSONObject("address").getString("phone"));
                                requestModel.setAddress_houseNumber(object.getJSONObject("json").getJSONObject("address").getString("houseNumber"));
                                requestModel.setAddress_streetAddress(object.getJSONObject("json").getJSONObject("address").getString("streetAddress"));
                                requestModel.setAddress_city(object.getJSONObject("json").getJSONObject("address").getString("city"));
                                requestModel.setAddress_country(object.getJSONObject("json").getJSONObject("address").getString("country"));
                                requestModel.setAddress_zipCode(object.getJSONObject("json").getJSONObject("address").getString("zipCode"));*/

                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < object.getJSONObject("json").getJSONArray("foodMenuItems").length(); j++) {
                                    JSONObject obj = object.getJSONObject("json").getJSONArray("foodMenuItems").getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                requestModel.setList(list_food);
                                list.add(requestModel);
                            }
                            if (list.size() > 0) {
                                adapter = new OrderRequestAdapter(context, list, HomeRestorentFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }


                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void ACCEPT_DECLINE_RESTAURANT_ORDER(String request_id, String status, String preparation_time, String delivery_by, String reason) {


        int position = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().equals(request_id))
                position = i;
        }
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, request_id);
        formBuilder.addFormDataPart(Parameters.STATUS, status);//1 => accept, 2 => decline
        formBuilder.addFormDataPart(Parameters.REASON, reason);
        formBuilder.addFormDataPart(Parameters.PREPARATION_TIME, preparation_time);
        formBuilder.addFormDataPart(Parameters.DELIVERYBY, delivery_by);//1 => restaurtant, 2 => delivery boy
        RequestBody formBody = formBuilder.build();
        int finalPosition = position;
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ACCEPT_DECLINE_RESTAURANT_ORDER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String s = "";
                        if (status.equals("1"))
                            s = "Request Accepted Successfully";
                        else
                            s = "Request Declined Successfully";

                        new IOSDialog.Builder(context)
                                .setCancelable(false)
                                .setMessage(s).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                list.get(finalPosition).setStatus(status);
                                adapter.notifyDataSetChanged();
                            }
                        }).show();
                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
