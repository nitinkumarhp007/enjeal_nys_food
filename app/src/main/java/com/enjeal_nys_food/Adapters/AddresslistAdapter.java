package com.enjeal_nys_food.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.enjeal_nys_food.Activities.AddressListingActivity;
import com.enjeal_nys_food.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddresslistAdapter extends RecyclerView.Adapter<AddresslistAdapter.RecyclerViewHolder> {
    AddressListingActivity context;
    LayoutInflater Inflater;

    private View view;

    public AddresslistAdapter(AddressListingActivity context) {
        this.context = context;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.address_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.tick.setVisibility(View.INVISIBLE);

      /*  if (list.get(position).isIs_select()) {
            holder.tick.setVisibility(View.VISIBLE);
        } else {
            holder.tick.setVisibility(View.INVISIBLE);
        }


        holder.name.setText(list.get(position).getName());
        holder.phone.setText(list.get(position).getMobile());
        holder.address.setText(list.get(position).getAddress_line_1() + " " + list.get(position).getCity());
        holder.cityPin.setText(list.get(position).getState() + "-" + list.get(position).getPin_code());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setIs_select(false);
                }
                list.get(position).setIs_select(true);
                notifyDataSetChanged();
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.city_pin)
        TextView cityPin;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.tick)
        ImageView tick;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
