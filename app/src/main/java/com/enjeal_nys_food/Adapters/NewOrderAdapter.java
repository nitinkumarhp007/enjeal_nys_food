package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Activities.OrderDetailActivity;
import com.enjeal_nys_food.Fragments.User.OrdersUserFragment;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NewOrderAdapter extends RecyclerView.Adapter<NewOrderAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    OrdersUserFragment ordersUserFragment;
    ArrayList<RequestModel> list;
    boolean is_order = false;

    public NewOrderAdapter(Context context, ArrayList<RequestModel> list,
                           OrdersUserFragment ordersUserFragment, boolean is_order) {
        this.context = context;
        this.is_order = is_order;
        this.list = list;
        this.ordersUserFragment = ordersUserFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.new_order_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

       /* if (position == 2)
            holder.order_type.setText("Home Delivery");*/

        holder.myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.myRecyclerView.setAdapter(new OrderItemsAdapter(context, list.get(position).getList()));

        holder.date_text.setText("Date:" + util.convertTimeStampDateTime(Long.parseLong(list.get(position).getCreated())));

        holder.name.setText(list.get(position).getUsername());
        Glide.with(context).load(list.get(position).getImage()).into(holder.image);
        holder.price.setText("Total: XAF" + list.get(position).getPrice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_order /*&& !list.get(position).getStatus().equals("2")*/) {
                    Intent intent = new Intent(context, OrderDetailActivity.class);
                    intent.putExtra("data", list.get(position));
                    intent.putExtra("is_user", true);
                    context.startActivity(intent);
                }

            }
        });


        /*holder.make_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ordersUserFragment.ORDER_PAYMENT(position);
            }
        });*/


        if (list.get(position).getStatus().equals("1")) {
            holder.status.setVisibility(View.VISIBLE);
            holder.status.setText("Accepted");
            holder.make_payment.setVisibility(View.VISIBLE);
            holder.status.setTextColor(context.getResources().getColor(R.color.colorAccent));

        } else {
            holder.status.setVisibility(View.VISIBLE);
            holder.make_payment.setVisibility(View.INVISIBLE);

            if (list.get(position).getStatus().equals("2")) {
                holder.status.setText("Declined");
                holder.status.setTextColor(context.getResources().getColor(R.color.red));
            } else {
                holder.status.setTextColor(context.getResources().getColor(R.color.colorAccent));
                holder.status.setText("Processing...");
            }


        }

        //  holder.make_payment


        holder.make_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("data", list.get(position));
                intent.putExtra("is_user", true);
                context.startActivity(intent);
            }
        }); /*holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               context.startActivity(new Intent(context, OrderDetailActivity.class));
           }
       });*/

        // 0=>pending 1=>accept 2=>decline 3=>paymentPlacedAndDeliveryBoyAssigned 4=>deliveryBoyPickedUp 5=>completed
        if (list.get(position).getStatus().equals("0")) {
            holder.make_payment.setVisibility(View.INVISIBLE);
            holder.status.setText("Requested");
            holder.date.setVisibility(View.INVISIBLE);
        } else if (list.get(position).getStatus().equals("1")) {
            holder.make_payment.setVisibility(View.VISIBLE);
            holder.status.setText("Accepted");
            holder.date.setVisibility(View.INVISIBLE);
        } else if (list.get(position).getStatus().equals("2")) {
            holder.status.setText("Declined");
            holder.make_payment.setVisibility(View.INVISIBLE);
            holder.date.setVisibility(View.INVISIBLE);

            holder.reason.setVisibility(View.INVISIBLE);
            holder.reason.setText(list.get(position).getReason());

        } else if (list.get(position).getStatus().equals("3")) {
            holder.status.setText("Order Placed");
            holder.make_payment.setVisibility(View.INVISIBLE);
            holder.date.setVisibility(View.INVISIBLE);
        } else if (list.get(position).getStatus().equals("4")) {
            holder.status.setText("Picked Up");
            holder.status.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } else if (list.get(position).getStatus().equals("5")) {
            holder.status.setText("Delivered");
            holder.make_payment.setVisibility(View.INVISIBLE);
            holder.date.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.my_recycler_view)
        RecyclerView myRecyclerView;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.make_payment)
        TextView make_payment;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.order_type)
        TextView order_type;
        @BindView(R.id.reason)
        TextView reason;
        @BindView(R.id.date_text)
        TextView date_text;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
