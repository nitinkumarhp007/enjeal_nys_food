package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Activities.OrderDetailActivity;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class NewOrdersRestorentAdapter extends RecyclerView.Adapter<NewOrdersRestorentAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<RequestModel> list;

    public NewOrdersRestorentAdapter(Context context, ArrayList<RequestModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.new_order_restorent_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.myRecyclerView.setAdapter(new OrderItemsAdapter(context, list.get(position).getList()));

        holder.date_text.setText("Date:" + util.convertTimeStampDateTime(Long.parseLong(list.get(position).getCreated())));


        holder.name.setText(list.get(position).getUsername());
        Glide.with(context).load(list.get(position).getImage()).into(holder.image);
        holder.price.setText("Total: XAF" + list.get(position).getPrice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("data", list.get(position));
                intent.putExtra("from_restaurant", true);
                context.startActivity(intent);
            }
        });
        holder.status.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        // 0=>pending 1=>accept 2=>decline 3=>paymentPlacedAndDeliveryBoyAssigned 4=>deliveryBoyPickedUp 5=>completed
        if (list.get(position).getStatus().equals("2")) {
            holder.status.setText("Declined");
            holder.status.setTextColor(context.getResources().getColor(R.color.red));
        } else if (list.get(position).getStatus().equals("3")) {
            holder.status.setText("Order Placed");
        } else if (list.get(position).getStatus().equals("4")) {
            holder.status.setText("Picked Up");
        } else if (list.get(position).getStatus().equals("5")) {
            holder.status.setText("Delivered");
        }

        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("data", list.get(position));
                intent.putExtra("from_restaurant", true);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.my_recycler_view)
        RecyclerView myRecyclerView;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.details)
        TextView details;
        @BindView(R.id.date)
        TextView date_text;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
