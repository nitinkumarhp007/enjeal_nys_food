package com.enjeal_nys_food.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Activities.OrderDetailActivity;
import com.enjeal_nys_food.Fragments.User.Restaurtant.HomeRestorentFragment;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderRequestAdapter extends RecyclerView.Adapter<OrderRequestAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<RequestModel> list;
    HomeRestorentFragment homeRestorentFragment;

    public OrderRequestAdapter(Context context, ArrayList<RequestModel> list, HomeRestorentFragment homeRestorentFragment) {
        this.context = context;
        this.list = list;
        this.homeRestorentFragment = homeRestorentFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.order_request_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.myRecyclerView.setAdapter(new OrderItemsAdapter(context, list.get(position).getList()));

        holder.date_text.setText("Date:" + util.convertTimeStampDateTime(Long.parseLong(list.get(position).getCreated())));

        holder.name.setText(list.get(position).getUsername());
        Glide.with(context).load(list.get(position).getImage()).into(holder.image);
        holder.price.setText("Total: XAF" + list.get(position).getPrice());


        if (list.get(position).getStatus().equals("1") || list.get(position).getStatus().equals("2")) {
            holder.accept.setVisibility(View.GONE);
            holder.decline.setVisibility(View.GONE);
            holder.status.setVisibility(View.VISIBLE);

            if (list.get(position).getStatus().equals("1")) {
                holder.status.setText("Accepted");
            } else if (list.get(position).getStatus().equals("2")) {
                holder.status.setText("Declined");
            }
        } else {
            holder.accept.setVisibility(View.VISIBLE);
            holder.decline.setVisibility(View.VISIBLE);
            holder.status.setVisibility(View.GONE);
        }


        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("data", list.get(position));
                intent.putExtra("from_restaurant", true);
                context.startActivity(intent);

            }
        });

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_dialog(position);
            }
        });

        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decline_show_dialog(position);
            }
        });

    }

    private void show_dialog(int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog);

        TextView submit = (TextView) dialog.findViewById(R.id.submit);
        EditText preparation_time_in_mins = (EditText) dialog.findViewById(R.id.preparation_time_in_mins);
        RadioButton delivery_expert = (RadioButton) dialog.findViewById(R.id.delivery_expert);
        RadioButton restaurant_delivery = (RadioButton) dialog.findViewById(R.id.restaurant_delivery);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ConnectivityReceiver.isConnected()) {
                    if (preparation_time_in_mins.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Preparation Time (in mins)");
                    } else {
                        String delivery_by = "";

                        if (restaurant_delivery.isChecked())
                            delivery_by = "1";
                        else
                            delivery_by = "2";

                        dialog.dismiss();

                        homeRestorentFragment.ACCEPT_DECLINE_RESTAURANT_ORDER(list.get(position).getId(), "1", preparation_time_in_mins.getText().toString().trim(), delivery_by, "");
                    }

                } else
                    util.IOSDialog(context, util.internet_Connection_Error);

            }
        });

        dialog.show();
    }

    private void decline_show_dialog(int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_decline);

        TextView submit = (TextView) dialog.findViewById(R.id.submit);
        EditText reason = (EditText) dialog.findViewById(R.id.reason);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    if (reason.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Reason");
                    } else {
                        dialog.dismiss();
                        homeRestorentFragment.ACCEPT_DECLINE_RESTAURANT_ORDER(list.get(position).getId(), "2", "", "1", reason.getText().toString());

                    }

                } else
                    util.IOSDialog(context, util.internet_Connection_Error);

            }
        });

        dialog.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.my_recycler_view)
        RecyclerView myRecyclerView;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.accept)
        TextView accept;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.decline)
        TextView decline;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.details)
        TextView details;
        @BindView(R.id.date_text)
        TextView date_text;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
