package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Activities.AddFoodActivity;
import com.enjeal_nys_food.Fragments.User.Restaurtant.MenuRestorentFragment;
import com.enjeal_nys_food.ModelClasses.CategoryModel;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.util;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeRestorentAdapter extends RecyclerView.Adapter<HomeRestorentAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<MenuItemModel> list;
    MenuRestorentFragment menuRestorentFragment;

    public HomeRestorentAdapter(Context context, ArrayList<MenuItemModel> list,
                                MenuRestorentFragment menuRestorentFragment) {
        this.context = context;
        this.list = list;
        this.menuRestorentFragment = menuRestorentFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.homerestorent_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getName());
        holder.category.setText(list.get(position).getCategory_name());
        holder.price.setText("XAF" + list.get(position).getPrice());
        holder.num_plate_left.setText("Number of plate left: " + list.get(position).getQuantity());

        Glide.with(context).load(list.get(position).getImage()).into(holder.image);

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuRestorentFragment.DeleteAlert(position, list);
            }
        });
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddFoodActivity.class);
                intent.putExtra("data", list.get(position));
                intent.putExtra("from_edit", true);
                context.startActivity(intent);
            }
        });

        if (list.get(position).getPromotionValid().equals("1")) {
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.promotional_price.setVisibility(View.VISIBLE);
            //   holder.promotional_price.setText("XAF25 (till 20-12-2020)");
            holder.promotional_price.setText("XAF" + list.get(position).getPromotionPrice() + " (till " + util.convertTimeStampDate(Long.parseLong(list.get(position).getPromotionDate())) + ")");
        } else {
            holder.promotional_price.setVisibility(View.INVISIBLE);
        }


        holder.promotion_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddFoodActivity.class);
                intent.putExtra("from_promotion", true);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddFoodActivity.class);
                intent.putExtra("from_edit", true);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        RoundedImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.promotion_price)
        ImageView promotion_price;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.promotional_price)
        TextView promotional_price;
        @BindView(R.id.num_plate_left)
        TextView num_plate_left;
        @BindView(R.id.edit_button)
        ImageView editButton;
        @BindView(R.id.delete_button)
        ImageView deleteButton;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<MenuItemModel> nList = new ArrayList<MenuItemModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (MenuItemModel wp : tempList) {
                if (wp.getName().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/

}
