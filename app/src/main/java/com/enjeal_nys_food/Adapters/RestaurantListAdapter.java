package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.enjeal_nys_food.Activities.RestorentDetailActivity;
import com.enjeal_nys_food.Activities.RestorentListActivity;
import com.enjeal_nys_food.Activities.SignInActivity;
import com.enjeal_nys_food.ModelClasses.UserModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.SavePref;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.RecyclerViewHolder> {
    RestorentListActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<UserModel> tempList;
    ArrayList<UserModel> list;

    private SavePref savePref;

    public RestaurantListAdapter(RestorentListActivity context, ArrayList<UserModel> list) {
        this.context = context;
        this.tempList = list;
        this.list = list;
        savePref = new SavePref(context);
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.home_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (list.get(position).getIs_fav().equals("1")) {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.i_heart_filled));
        } else {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.i_heart));
        }

        holder.name.setText(list.get(position).getName());
        holder.distance.setText("Open (" + list.get(position).getDistance() + ")");
        if (!list.get(position).getAvgRating().isEmpty())
            holder.ratingBar.setRating(Float.parseFloat(list.get(position).getAvgRating()));


     /*   if (list.get(position).getOnline_status().equals("1"))
            holder.distance.setText("Open (" + list.get(position).getDistance() + ")");
        else
            holder.distance.setText("Closed (" + list.get(position).getDistance() + ")");*/
        if (list.get(position).getOnline_status().equals("1"))
            holder.distance.setText("Open");
        else
            holder.distance.setText("Closed");


        Glide.with(context)
                .load(list.get(position).getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.image);


        holder.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (savePref.getAuthorization_key().isEmpty()) {
                    login_first(context);
                } else {
                    context.FAV_UNFAV_RESTAURANT(position);
                }

            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RestorentDetailActivity.class);
                intent.putExtra("id", list.get(position).getId());
                context.startActivity(intent);
            }
        });

    }

    public void login_first(Context context) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("You need to Login First").setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, SignInActivity.class);
                intent.putExtra("from_home", true);
                context.startActivity(intent);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.heart)
        ImageView heart;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.distance)
        TextView distance;
        @BindView(R.id.rating_bar)
        RatingBar ratingBar;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<UserModel> nList = new ArrayList<UserModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (UserModel wp : tempList) {
                if (wp.getName().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }


}
