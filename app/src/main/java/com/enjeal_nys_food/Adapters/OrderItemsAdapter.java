package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<MenuItemModel> list;

    public OrderItemsAdapter(Context context, ArrayList<MenuItemModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.order_item_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getName());
        holder.price.setText("XAF" + list.get(position).getPrice());

        if (list.get(position).getPromotionValid().equals("1")) {
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.promotional_price.setVisibility(View.VISIBLE);
            //   holder.promotional_price.setText("XAF25 (till 20-12-2020)");
            holder.promotional_price.setText("XAF" + list.get(position).getPromotionPrice() + " (till " + util.convertTimeStampDate(Long.parseLong(list.get(position).getPromotionDate())) + ")");
        } else {
            holder.promotional_price.setVisibility(View.INVISIBLE);
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.START_HYPHEN_EDIT_NO_EDIT);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.promotional_price)
        TextView promotional_price;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
