package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enjeal_nys_food.Activities.RestorentDetailActivity;
import com.enjeal_nys_food.ModelClasses.CategoryModel;
import com.enjeal_nys_food.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PrimaryFoodAdapter extends RecyclerView.Adapter<PrimaryFoodAdapter.RecyclerViewHolder> {
    RestorentDetailActivity context;
    LayoutInflater Inflater;
    private View view;
    boolean is_detail = false;
    ArrayList<CategoryModel> list;

    public PrimaryFoodAdapter(RestorentDetailActivity context, boolean is_detail, ArrayList<CategoryModel> list) {
        this.context = context;
        this.list = list;
        this.is_detail = is_detail;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.primary_food_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getName());

        holder.my_recycler_view.setVisibility(View.GONE);

        holder.my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        holder.my_recycler_view.setAdapter(new FoodAdapter(context, is_detail, list.get(position).getList(), list, position));


        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.my_recycler_view.getVisibility() == View.GONE)
                    holder.my_recycler_view.setVisibility(View.VISIBLE);
                else
                    holder.my_recycler_view.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.my_recycler_view)
        RecyclerView my_recycler_view;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
