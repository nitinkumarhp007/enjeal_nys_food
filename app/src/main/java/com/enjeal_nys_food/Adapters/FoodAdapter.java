package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Activities.RestorentDetailActivity;
import com.enjeal_nys_food.ModelClasses.CategoryModel;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.util;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.RecyclerViewHolder> {
    RestorentDetailActivity context;
    LayoutInflater Inflater;
    private View view;
    boolean is_detail;
    ArrayList<MenuItemModel> list;
    ArrayList<CategoryModel> main_list;
    int mainposition;

    public FoodAdapter(RestorentDetailActivity context, boolean is_detail, ArrayList<MenuItemModel> list, ArrayList<CategoryModel> main_list, int mainposition) {
        this.context = context;
        this.list = list;
        this.main_list = main_list;
        this.mainposition = mainposition;
        this.is_detail = is_detail;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.food_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        if (list.get(position).getPromotionValid().equals("1")) {
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.promotional_price.setVisibility(View.VISIBLE);
            //   holder.promotional_price.setText("$25 (till 20-12-2020)");
            holder.promotional_price.setText("XAF" + list.get(position).getPromotionPrice() + " (till " + util.convertTimeStampDate(Long.parseLong(list.get(position).getPromotionDate())) + ")");
        } else {
            holder.promotional_price.setVisibility(View.INVISIBLE);
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.START_HYPHEN_EDIT_NO_EDIT);
        }


        if (is_detail)
            holder.checkbox.setVisibility(View.INVISIBLE);

        holder.name.setText(list.get(position).getName());
        holder.quanity.setText("Number of plate left: " + list.get(position).getQuantity());
        holder.price.setText("XAF" + list.get(position).getPrice());

        Glide.with(context).load(list.get(position).getImage()).into(holder.image);


        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    main_list.get(mainposition).getList().get(position).setIs_select(true);
                else
                    main_list.get(mainposition).getList().get(position).setIs_select(false);

                int total = 0;
                for (int j = 0; j < main_list.size(); j++) {
                    ArrayList<MenuItemModel> list = main_list.get(j).getList();
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).isIs_select()) {
                            if (list.get(position).getPromotionValid().equals("1"))
                                total = total + Integer.parseInt(list.get(i).getPromotionPrice());
                            else
                                total = total + Integer.parseInt(list.get(i).getPrice());

                        }
                    }

                }
                context.checkout.setText("Check Out (XAF" + total + ")");
                context.total = String.valueOf(total);

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        RoundedImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.promotional_price)
        TextView promotional_price;
        @BindView(R.id.quanity)
        TextView quanity;
        @BindView(R.id.checkbox__)
        CheckBox checkbox;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
