package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Activities.OrderDetailDeliveryBoyActivity;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.util;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DeliveryExpertOngoingOrdersAdapter extends RecyclerView.Adapter<DeliveryExpertOngoingOrdersAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<RequestModel> list;

    public DeliveryExpertOngoingOrdersAdapter(Context context, ArrayList<RequestModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.layout_new_orders, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getUser_name());
        holder.address.setText("Pickup Address :" + list.get(position).getRestaurant_address());
        Glide.with(context).load(list.get(position).getUser_image()).into(holder.image);

        holder.date_text.setText("Date:" + util.convertTimeStampDateTime(Long.parseLong(list.get(position).getCreated())));

        // 0=>pending 1=>accept 2=>decline 3=>paymentPlacedAndDeliveryBoyAssigned 4=>deliveryBoyPickedUp 5=>completed
        if (list.get(position).getStatus().equals("5")) {
            holder.status.setVisibility(View.VISIBLE);
        } else {
            holder.status.setVisibility(View.INVISIBLE);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailDeliveryBoyActivity.class);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
                //overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });
        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailDeliveryBoyActivity.class);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        RoundedImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.details)
        TextView details;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.date_text)
        TextView date_text;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
