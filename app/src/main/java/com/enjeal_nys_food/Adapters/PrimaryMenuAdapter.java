package com.enjeal_nys_food.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enjeal_nys_food.Fragments.User.Restaurtant.MenuRestorentFragment;
import com.enjeal_nys_food.ModelClasses.CategoryModel;
import com.enjeal_nys_food.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PrimaryMenuAdapter extends RecyclerView.Adapter<PrimaryMenuAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    boolean is_detail = false;
    ArrayList<CategoryModel> list;
    MenuRestorentFragment menuRestorentFragment;

    public PrimaryMenuAdapter(Context context, boolean is_detail, ArrayList<CategoryModel> list,
                              MenuRestorentFragment menuRestorentFragment) {
        this.context = context;
        this.is_detail = is_detail;
        this.list = list;
        this.menuRestorentFragment = menuRestorentFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.primary_menu_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.my_recycler_view.setVisibility(View.GONE);

        holder.my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        holder.my_recycler_view.setAdapter(new HomeRestorentAdapter(context, list.get(position).getList(), menuRestorentFragment));

        holder.name.setText(list.get(position).getName());


        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.my_recycler_view.getVisibility() == View.GONE)
                    holder.my_recycler_view.setVisibility(View.VISIBLE);
                else
                    holder.my_recycler_view.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.my_recycler_view)
        RecyclerView my_recycler_view;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
