package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Adapters.PrimaryFoodAdapter;
import com.enjeal_nys_food.Fragments.User.FavFoodUserFragment;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.ModelClasses.CategoryModel;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.GPSTracker;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.enjeal_nys_food.parser.GetAsyncGet;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RestorentDetailActivity extends AppCompatActivity {

    RestorentDetailActivity context;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.fav_button)
    ImageView favButton;
    @BindView(R.id.checkout)
    public TextView checkout;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.distance)
    TextView distance;
    @BindView(R.id.estimated_time)
    TextView estimatedTime;
    @BindView(R.id.get_direction)
    TextView getDirection;
    private SavePref savePref;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.rating_bar_layout)
    LinearLayout rating_bar_layout;
    @BindView(R.id.rating)
    TextView rating;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    String latitude_res = "", longitude_res = "", latitude = "", longitude = "", image_text = "", phone;
    String id = "", isFav = "";
    GPSTracker gpsTracker = null;

    public String time_d = "", distance_text_top = "", online_status = "", total = "";


    private ArrayList<CategoryModel> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restorent_detail);
        ButterKnife.bind(this);

        context = RestorentDetailActivity.this;
        savePref = new SavePref(context);

        id = getIntent().getStringExtra("id");
        distance_text_top = getIntent().getStringExtra("distance");

        gpsTracker = new GPSTracker(context);


        setToolbar();

        if (ConnectivityReceiver.isConnected())
            GETRESTAURRANTPROFILE();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void GETRESTAURRANTPROFILE() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, id);
        formBuilder.addFormDataPart(Parameters.LATITUDE, String.valueOf(gpsTracker.getLatitude()));
        formBuilder.addFormDataPart(Parameters.LONGITUDE, String.valueOf(gpsTracker.getLongitude()));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.RESTAURANTDETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject restaurant = jsonmainObject.getJSONObject("body");
                            JSONObject body = restaurant.getJSONObject("restaurant");
                            name.setText(body.getString("name"));
                            image_text = body.getString("image");
                            phone = body.getString("phone");
                            Glide.with(context).load(body.getString("image")).into(image);
                            ratingBar.setRating(Float.parseFloat(body.getString("avgRating")));

                            if (body.getString("avgRating").equals("0.0000"))
                                rating.setText("0");
                            else
                                rating.setText(body.optString("avgRating"));

                            isFav = body.optString("isFavourite");
                            latitude_res = body.getString("latitude");
                            longitude_res = body.getString("longitude");
                            if (isFav.equals("1")) {
                                favButton.setImageDrawable(context.getResources().getDrawable(R.drawable.i_heart_filled));
                            } else {
                                favButton.setImageDrawable(context.getResources().getDrawable(R.drawable.i_heart));
                            }
                            online_status = body.optString(Parameters.ONLINE_STATUS);

                            //String distance_text = body.optString("distance");

                            GetDistanceAPI(mDialog);









                            /*if (!latitude_res.isEmpty()) {
                                if (gpsTracker.canGetLocation()) {
                                    distance.setText("(" + String.valueOf(distance(Double.parseDouble(latitude_res), Double.parseDouble(longitude_res)
                                            , gpsTracker.getLatitude(), gpsTracker.getLongitude())) + " KM)");


                                    double dis = distance(Double.parseDouble(latitude_res), Double.parseDouble(longitude_res)
                                            , gpsTracker.getLatitude(), gpsTracker.getLongitude());

                                    double time__ = dis / 40;

                                    estimatedTime.setText("Estimated Time : " + time__ + " mins");

                                }
                            }
*/
                            JSONArray foodMenu = restaurant.getJSONArray("foodMenu");

                            for (int i = 0; i < foodMenu.length(); i++) {
                                JSONObject object = foodMenu.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.getString("image"));
                                JSONArray foodMenuItems = object.getJSONArray("foodMenuItems");
                                ArrayList<MenuItemModel> list_food = new ArrayList<>();
                                for (int j = 0; j < foodMenuItems.length(); j++) {
                                    JSONObject obj = foodMenuItems.getJSONObject(j);
                                    MenuItemModel menuItemModel = new MenuItemModel();
                                    menuItemModel.setId(obj.getString("id"));
                                    menuItemModel.setName(obj.getString("name"));
                                    menuItemModel.setImage(obj.getString("image"));
                                    menuItemModel.setDescription(obj.optString("description"));
                                    menuItemModel.setPrice(obj.getString("price"));
                                    menuItemModel.setCategory_name(object.optString("name"));
                                    menuItemModel.setQuantity(obj.optString("quantity"));
                                    menuItemModel.setCategory_id(obj.optString("categoryId"));
                                    menuItemModel.setPromotionPrice(obj.optString("promotionPrice"));
                                    menuItemModel.setPromotionDate(obj.optString("promotionDate"));
                                    menuItemModel.setPromotionValid(obj.optString("promotionValid"));
                                    list_food.add(menuItemModel);
                                }
                                categoryModel.setList(list_food);
                                list.add(categoryModel);
                            }

                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(new PrimaryFoodAdapter(context, false, list));


                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void GetDistanceAPI(ProgressDialog mDialog) {
        //30.7333 76.7794
        latitude = String.valueOf(gpsTracker.getLatitude());
        longitude = String.valueOf(gpsTracker.getLongitude());

        Log.e("resulttttt", latitude_res.substring(0, latitude_res.length() - 4));

        String matrix_url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + latitude + "," + longitude +
                "&destinations=" + latitude_res + "," + longitude_res + "&key=" + getResources().getString(R.string.APISERVERKEY);
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FOODCATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, matrix_url, formBody) {
            @Override
            public void getValueParse(String result) {
                try {
                    if (mDialog != null)
                        mDialog.dismiss();
                } catch (Exception e) {
                }

                Log.e("resulttttt", result);
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        String distance_text = "", duration = "";


                        if (jsonmainObject.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getString("status").equals("OK")) {
                            distance_text = jsonmainObject.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("distance").getString("text");
                            duration = jsonmainObject.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("duration").getString("text");
                        } else {
                            distance_text = distance_text_top;
                            duration = "22 Mins";
                        }


                        if (online_status.equals("1")) {
                            distance.setText("Open (" + distance_text + ")");
                            distance.setTextColor(getResources().getColor(R.color.green));
                        } else {
                            distance.setText("Close (" + distance_text + ")");
                            distance.setTextColor(getResources().getColor(R.color.red));
                        }

                        estimatedTime.setText("Estimated time: " + duration);
                        estimatedTime.setVisibility(View.VISIBLE);


                        scrollView.setVisibility(View.VISIBLE);


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Restaurant");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @OnClick({R.id.fav_button, R.id.rating_bar_layout, R.id.checkout, R.id.get_direction})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fav_button:
                if (savePref.getAuthorization_key().isEmpty()) {
                    login_first(this);
                } else {
                    FAV_UNFAV_RESTAURANT();
                }

                break;
            case R.id.rating_bar_layout:
                if (savePref.getAuthorization_key().isEmpty()) {
                    login_first(this);
                } else {
                    Intent intent1 = new Intent(context, ReviewListActivity.class);
                    // intent.putExtra("category_id", category_list.get(position).getId());
                    //intent.putExtra("category_name", category_list.get(position).getName());
                    startActivity(intent1);
                }

                break;
            case R.id.checkout:
                if (savePref.getAuthorization_key().isEmpty()) {
                    login_first(this);
                } else {
                    if (online_status.equals("1")) {
                        ArrayList<MenuItemModel> list_selected = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            for (int j = 0; j < list.get(i).getList().size(); j++) {

                                if (list.get(i).getList().get(j).isIs_select()) {
                                    list_selected.add(list.get(i).getList().get(j));
                                }

                            }
                        }

                        if (list_selected.size() > 0) {
                            Intent intent = new Intent(context, CheckOutActivity.class);
                            intent.putExtra("name", name.getText().toString().trim());
                            intent.putExtra("phone", phone);
                            intent.putExtra("image_text", image_text);
                            intent.putExtra("total", total);
                            intent.putExtra("restaurtant_id", id);
                            intent.putExtra("list_selected", list_selected);
                            intent.putExtra("latitude_res", latitude_res);
                            intent.putExtra("longitude_res", longitude_res);
                            intent.putExtra("time_d", time_d);
                            startActivity(intent);
                        } else {
                            util.IOSDialog(context, "Please select food to order");
                        }


                    } else {
                        util.IOSDialog(context, "Restaurtant is closed for receiving any orders.");
                    }
                }
                break;
            case R.id.get_direction:
                if (!latitude_res.equals("0.0"))
                    loadNavigationView(latitude_res, longitude_res);
                break;
        }
    }

    public void login_first(Context context) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("You need to Login First").setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, SignInActivity.class);
                intent.putExtra("from_home", true);
                context.startActivity(intent);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public void loadNavigationView(String lat, String lng) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("dir")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("destination", Double.parseDouble(lat) + "," + Double.parseDouble(lng));
        String url = builder.build().toString();
        Log.e("Directions", url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url + "&mode=driving"));
        startActivity(i);
    }

    public void FAV_UNFAV_RESTAURANT() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.RESTAURANTID, id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.FAV_UNFAV_RESTAURANT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {

                            if (isFav.equals("1")) {
                                isFav = "0";
                                favButton.setImageDrawable(context.getResources().getDrawable(R.drawable.i_heart));
                            } else {
                                isFav = "1";
                                favButton.setImageDrawable(context.getResources().getDrawable(R.drawable.i_heart_filled));
                            }
                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
