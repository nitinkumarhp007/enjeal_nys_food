package com.enjeal_nys_food.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.GPSTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class FetchLocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveCanceledListener,
        GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraMoveListener  {

    FetchLocationActivity context;
    @BindView(R.id.search_bar)
    TextView searchBar;
    @BindView(R.id.pin_location)
    Button pinLocation;
    private GoogleMap mMap;
    private float currentZoom = 15;
    GPSTracker gpsTracker = null;
    String latitude = "", longitude = "";
    String latitude_current = "", longitude_current = "";

    GoogleApiClient googleApiClient;


    Location location = null;
    FusedLocationProviderClient fusedLocationClient = null;

    @BindView(R.id.icon8_current)
    ImageView icon8_current;


    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_location);

        ButterKnife.bind(this);
        context=FetchLocationActivity.this;


        gpsTracker = new GPSTracker(context);

        if (gpsTracker.canGetLocation()) {
            //searchBar.setText(getCompleteAddressString(gpsTracker.getLatitude(), gpsTracker.getLongitude()));


            latitude = String.valueOf(gpsTracker.getLatitude());
            latitude_current = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
            longitude_current = String.valueOf(gpsTracker.getLongitude());


            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Log.e("dataaa", "address_ " + address_);
                Log.e("dataaa", "city " + city);
                Log.e("dataaa", "state " + state);
                Log.e("dataaa", "country " + country);
                Log.e("dataaa", "postalCode " + postalCode);
                Log.e("dataaa", "knownName " + knownName);

                Log.e("dataaa", "subadminaera " + addresses.get(0).getSubAdminArea());
                Log.e("dataaa", "subalocality " + addresses.get(0).getSubLocality());


                searchBar.setText(address_);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }


            Log.e("current_location-", String.valueOf(gpsTracker.getLatitude()) + " " + String.valueOf(gpsTracker.getLongitude()));
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.proselectlocationmapid);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000,
                10, locationListenerGPS);


        pinLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                intent.putExtra("location", searchBar.getText().toString().trim());
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        icon8_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng latLng = new LatLng(Double.parseDouble(latitude_current),
                        Double.parseDouble(longitude_current));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        latLng, currentZoom);
                Log.e("ZOOM__", String.valueOf(currentZoom));
                mMap.animateCamera(location, 2100, null);


                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(context, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    Log.e("dataaa", "address_ " + address_);
                    Log.e("dataaa", "city " + city);
                    Log.e("dataaa", "state " + state);
                    Log.e("dataaa", "country " + country);
                    Log.e("dataaa", "postalCode " + postalCode);
                    Log.e("dataaa", "knownName " + knownName);

                    Log.e("dataaa", "subadminaera " + addresses.get(0).getSubAdminArea());
                    Log.e("dataaa", "subalocality " + addresses.get(0).getSubLocality());


                    searchBar.setText(address_);


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        

        if (!gpsTracker.canGetLocation())
            EnableGPSAutoMatically();
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
                            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    Activity#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for Activity#requestPermissions for more details.
                                return;
                            }
                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());


                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(context, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }


                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                location = locationList.get(locationList.size() - 1);




                searchBar.setText(getCompleteAddressString(location.getLatitude(), location.getLongitude()));

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(context, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    Log.e("dataaa", "address_ " + address_);
                    Log.e("dataaa", "city " + city);
                    Log.e("dataaa", "state " + state);
                    Log.e("dataaa", "country " + country);
                    Log.e("dataaa", "postalCode " + postalCode);
                    Log.e("dataaa", "knownName " + knownName);

                    Log.e("dataaa", "subadminaera " + addresses.get(0).getSubAdminArea());
                    Log.e("dataaa", "subalocality " + addresses.get(0).getSubLocality());


                    searchBar.setText(address_);


                } catch (IOException e) {
                    e.printStackTrace();
                }

                latitude = String.valueOf(location.getLatitude());
                latitude_current = String.valueOf(location.getLatitude());
                longitude = String.valueOf(location.getLongitude());
                longitude_current = String.valueOf(location.getLongitude());

                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                CameraUpdate location11 = CameraUpdateFactory.newLatLngZoom(
                        latLng, 13);
                mMap.animateCamera(location11, 2100, null);


                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };


    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            String msg = "New Latitude: " + latitude + "New Longitude: " + longitude;
            //   Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

            // Log.e("callllll", msg);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraIdle() {

    }

    @Override
    public void onCameraMoveCanceled() {

    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraMoveStarted(int i) {

        if (i == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
           /* Toast.makeText(context, "The user gestured on the map.",
                    Toast.LENGTH_SHORT).show();*/


        } else if (i == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
           /* Toast.makeText(context, "The user tapped something on the map.",
                    Toast.LENGTH_SHORT).show();*/
        } else if (i == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            /*Toast.makeText(context, "The app moved the camera.",
                    Toast.LENGTH_SHORT).show();*/
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (gpsTracker.canGetLocation()) {
            LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    latLng, 13);
            mMap.animateCamera(location, 2100, null);

            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    //get latlng at the center by calling
                    LatLng midLatLng = mMap.getCameraPosition().target;

                    Log.e("callllll", "end" + String.valueOf(midLatLng.latitude) + "  " + String.valueOf(midLatLng.longitude));

                    latitude = String.valueOf(midLatLng.latitude);
                    longitude = String.valueOf(midLatLng.longitude);


                    searchBar.setText(getCompleteAddressString(midLatLng.latitude, midLatLng.longitude));


                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(context, Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(midLatLng.latitude, midLatLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();

                        Log.e("dataaa", "address_ " + address_);
                        Log.e("dataaa", "city " + city);
                        Log.e("dataaa", "state " + state);
                        Log.e("dataaa", "country " + country);
                        Log.e("dataaa", "postalCode " + postalCode);
                        Log.e("dataaa", "knownName " + knownName);

                        Log.e("dataaa", "subadminaera " + addresses.get(0).getSubAdminArea());
                        Log.e("dataaa", "subalocality " + addresses.get(0).getSubLocality());


                        searchBar.setText(address_);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            });

            // mMap.setOnCameraIdleListener(this);
            mMap.setOnCameraMoveStartedListener(this);
            mMap.setOnCameraMoveListener(this);
            mMap.setOnCameraMoveCanceledListener(this);

        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                // Log.e("My Current loction address", strReturnedAddress.toString());
            } else {
                // Log.e("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }


    @Override
    public void onLocationChanged(Location location__) {
        location = location__;

        searchBar.setText(getCompleteAddressString(location.getLatitude(), location.getLongitude()));

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(location__.getLatitude(), location__.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            Log.e("dataaa", "address_ " + address_);
            Log.e("dataaa", "city " + city);
            Log.e("dataaa", "state " + state);
            Log.e("dataaa", "country " + country);
            Log.e("dataaa", "postalCode " + postalCode);
            Log.e("dataaa", "knownName " + knownName);

            Log.e("dataaa", "subadminaera " + addresses.get(0).getSubAdminArea());
            Log.e("dataaa", "subalocality " + addresses.get(0).getSubLocality());


            searchBar.setText(address_);


        } catch (IOException e) {
            e.printStackTrace();
        }

        latitude = String.valueOf(location.getLatitude());
        latitude_current = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
        longitude_current = String.valueOf(location.getLongitude());

        Log.e("onlocation_", String.valueOf(location.getLatitude() + "  " + String.valueOf(location.getLongitude())));
    }
}