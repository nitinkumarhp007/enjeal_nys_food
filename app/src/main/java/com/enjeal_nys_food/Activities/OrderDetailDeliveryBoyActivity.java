package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blikoon.qrcodescanner.QrCodeActivity;
import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Adapters.OrderItemsAdapter;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OrderDetailDeliveryBoyActivity extends AppCompatActivity {
    OrderDetailDeliveryBoyActivity context;
    @BindView(R.id.pickup)
    TextView pickup;
    private SavePref savePref;
    @BindView(R.id.name_r)
    TextView name_r;
    @BindView(R.id.pickup_address)
    TextView pickup_address;
    @BindView(R.id.image_r)
    RoundedImageView image_r;
    @BindView(R.id.image_user)
    RoundedImageView image_user;
    @BindView(R.id.name_user)
    TextView name_user;
    @BindView(R.id.delivery_address)
    TextView delivery_address;
    @BindView(R.id.estimated_time)
    TextView estimated_time;
    @BindView(R.id.estimated_time_preprare_food)
    TextView estimated_time_preprare_food;
    @BindView(R.id.total_amount_to_pay)
    TextView total_amount_to_pay;
    @BindView(R.id.get_direction)
    TextView get_direction;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.commission)
    TextView commission;
    @BindView(R.id.delivery_charges)
    TextView delivery_charges;
    @BindView(R.id.total_amount_to_pay_)
    TextView total_amount_to_pay_;
    @BindView(R.id.expected_time_of_delivery_layout)
    LinearLayout expected_time_of_delivery_layout;
    @BindView(R.id.expected_time_of_delivery)
    TextView expectedTimeOfDelivery;


    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    String status_text = "";
    String order_id = "";
    RequestModel requestModel;
    private static final int REQUEST_CODE_QR_SCAN = 101;

    String lat = "", lng = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_delivery_boy);
        ButterKnife.bind(this);

        context = OrderDetailDeliveryBoyActivity.this;
        savePref = new SavePref(context);

        requestModel = getIntent().getExtras().getParcelable("data");

        order_id = requestModel.getId();

        status_text = requestModel.getStatus();

        if (status_text.equals("3")) {
            pickup.setText("Pickup");

            lat = requestModel.getRestaurant_lat();
            lng = requestModel.getRestaurant_lng();

        } else if (status_text.equals("4")) {
            pickup.setText("Deliver");

            lat = requestModel.getLatitude();
            lng = requestModel.getLongitude();

        } else if (status_text.equals("5")) {
            pickup.setText("Delivered");
            pickup.setBackgroundColor(getResources().getColor(R.color.green));

            lat = requestModel.getLatitude();
            lng = requestModel.getLongitude();
        }


        name_r.setText(requestModel.getRestaurant_name());
        pickup_address.setText("Pickup Address: " + requestModel.getRestaurant_address());
        Glide.with(context).load(requestModel.getRestaurant_image()).into(image_r);
        Glide.with(context).load(requestModel.getUser_image()).into(image_user);
        name_user.setText(requestModel.getUser_name());
        delivery_address.setText("Delivery Address: " + requestModel.getLocation());
        total_amount_to_pay.setText("XAF" + requestModel.getPrice());

        myRecyclerView.setLayoutManager(new LinearLayoutManager(OrderDetailDeliveryBoyActivity.this));
        myRecyclerView.setAdapter(new OrderItemsAdapter(OrderDetailDeliveryBoyActivity.this, requestModel.getList()));

        if (requestModel.getExpectedTimeDelivery().isEmpty())
            expected_time_of_delivery_layout.setVisibility(View.GONE);
        else {
            expected_time_of_delivery_layout.setVisibility(View.VISIBLE);
            expectedTimeOfDelivery.setText(requestModel.getExpectedTimeDelivery());
        }


        price.setText("Price: XAF" + requestModel.getInitialcost());
        commission.setText("Commission: XAF" + requestModel.getCommission());
        delivery_charges.setText("Delivery Charges: XAF" + requestModel.getDeliverycharges());
        total_amount_to_pay_.setText("Total Payable: XAF" + requestModel.getPrice());

        estimated_time.setText("Estimated Time (Restaurant to User): " + requestModel.getEstimatedDeliveryTime() + " min");
        estimated_time_preprare_food.setText("Estimated Time (Food will be ready): " + requestModel.getPreparationTime() + " min");


        setToolbar();

        // 0=>pending 1=>accept 2=>decline 3=>paymentPlacedAndDeliveryBoyAssigned 4=>deliveryBoyPickedUp 5=>completed


        get_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!lat.equals("0.0"))
                    loadNavigationView(lat, lng);
            }
        });
    }

    public void loadNavigationView(String lat, String lng) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("dir")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("destination", Double.parseDouble(lat) + "," + Double.parseDouble(lng));
        String url = builder.build().toString();
        Log.e("Directions", url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url + "&mode=driving"));
        startActivity(i);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Order Detail");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @OnClick(R.id.pickup)
    public void onClick() {
        if (!status_text.equals("5")) {
            if (status_text.equals("3")) {
                new IOSDialog.Builder(context)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage("Are you sure?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ORDER_PICK_FROMRESTAURANT();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            } else if (status_text.equals("4")) {
                //deliverr api
                new IOSDialog.Builder(context)
                        .setTitle(context.getResources().getString(R.string.app_name))
                        .setCancelable(false)
                        .setMessage("Ask Client to show QR Image to deliver the order!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(context, QrCodeActivity.class), REQUEST_CODE_QR_SCAN);
                    }
                })
                        /* .setNegativeButton("Cancel", null)*/.show();


            }
        }


    }

    //3=>paymentPlacedAndDeliveryBoyAssigned 4=>deliveryBoyPickedUp 5=>completed
    public void ORDER_PICK_FROMRESTAURANT() {
        String url = "";
        if (status_text.equals("3")) {
            url = AllAPIS.ORDERPICKUPFROMRESTAURANT;
        } else if (status_text.equals("4")) {
            url = AllAPIS.ORDERCOMPLETEBYDELIVERYBOY;
        }
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ORDERID, requestModel.getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, url, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);


                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            String s = "";
                            if (status_text.equals("3")) {
                                s = "Order Pickup Successfully!";
                                pickup.setText("Deliver");
                                status_text = "4";

                                lat = requestModel.getLatitude();
                                lng = requestModel.getLongitude();

                            } else if (status_text.equals("4")) {
                                s = "Order Delivered Successfully!";
                                pickup.setText("Delivered");
                                status_text = "5";
                                pickup.setBackgroundColor(getResources().getColor(R.color.green));
                            }


                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(s).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("message"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_QR_SCAN && resultCode == RESULT_OK) {
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");

            if (result.equals(order_id)) {
                //success
                ORDER_PICK_FROMRESTAURANT();
            } else {
                util.IOSDialog(context, "Scanned QR code not matched with current order.Try QR code of current order");
            }

        } else if (resultCode != Activity.RESULT_OK) {
            Log.d("LOGTAG", "COULD NOT GET A GOOD RESULT.");
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (result != null) {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }

    }
}
