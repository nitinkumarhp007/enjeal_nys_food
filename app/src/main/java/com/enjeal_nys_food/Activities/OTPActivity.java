package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.enjeal_nys_food.DeliveryExpertHomeActivity;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.RestaurtantMainActivity;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OTPActivity extends AppCompatActivity {
    OTPActivity context;
    private SavePref savePref;
    @BindView(R.id.num1)
    EditText num1;
    @BindView(R.id.num2)
    EditText num2;
    @BindView(R.id.num3)
    EditText num3;
    @BindView(R.id.num4)
    EditText num4;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.back_button)
    ImageView backButton;

    String authorization_key = "",  user_type = "", phone = "", country = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p);
        ButterKnife.bind(this);

        context = OTPActivity.this;
        savePref = new SavePref(context);

        go_next_auto();


    }

    @OnClick(R.id.submit)
    public void onClick() {
        OTPTASK();
    }

    private void OTPTASK() {
        if (ConnectivityReceiver.isConnected()) {
            if (num1.getText().toString().isEmpty() || num2.getText().toString().isEmpty() ||
                    num3.getText().toString().isEmpty() || num4.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter One Time Password(OTP)");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                String entered_otp = num1.getText().toString() + num2.getText().toString() + num3.getText().toString() + num4.getText().toString();
                VERIFY_OTP_API(entered_otp);
            }
        } else
            util.showToast(context, util.internet_Connection_Error);
    }

    private void VERIFY_OTP_API(String otp) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OTP, otp);
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "0");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.VERIFY_OTP, formBody, authorization_key) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setAuthorization_key(body.getString("token"));
                            savePref.setID(body.getString("id"));
                            savePref.setName(body.optString("name"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setImage(body.getString("image"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setUser_type(body.getString("role"));
                            savePref.setStringLatest(Parameters.COUNTRY_CODE, body.optString(Parameters.COUNTRY_CODE));

                            savePref.setStringLatest(Parameters.ADDRESS, body.optString(Parameters.ADDRESS));
                            savePref.setStringLatest(Parameters.LATITUDE, body.optString(Parameters.LATITUDE));
                            savePref.setStringLatest(Parameters.LONGITUDE, body.optString(Parameters.LONGITUDE));
                            savePref.setStringLatest(Parameters.RESTAURTANTLICENSEIMAGE, body.optString(Parameters.RESTAURTANTLICENSEIMAGE));

                            savePref.setStringLatest("notification_switch", body.optString(Parameters.ONLINE_STATUS));
                            savePref.setStringLatest(Parameters.DELIVERYEXPERTIDIMAGE, body.optString(Parameters.DELIVERYEXPERTIDIMAGE));

                            if (savePref.getUser_type().equals("1")) {
                                util.showToast(context, "Welcome to " + getResources().getString(R.string.app_name));

                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                            } else if (savePref.getUser_type().equals("2") || savePref.getUser_type().equals("3")) {
                                String title = "";
                                if (savePref.getUser_type().equals("2"))
                                    title = "Restaurtant Register Sucessfully! Now wait for Admin's Approval!";
                                else
                                    title = "Delivery Expert Register Sucessfully! Now wait for Admin's Approval!";

                                new IOSDialog.Builder(context)
                                        .setTitle(context.getResources().getString(R.string.app_name))
                                        .setCancelable(false)
                                        .setMessage(title).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(context, SignInActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                    }
                                }).show();
                            }



                          /*  Intent intent = new Intent(context, SignUpActivity.class);
                            intent.putExtra("authorization_key", authorization_key);
                            intent.putExtra("user_type", user_type);
                            intent.putExtra("phone", phone);
                            intent.putExtra("country", country);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/

                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void go_next_auto() {
        authorization_key = getIntent().getStringExtra("authorization_key");
        user_type = getIntent().getStringExtra("user_type");
        phone = getIntent().getStringExtra("phone");
        country = getIntent().getStringExtra("country");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        num1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num2.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num3.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num4.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    util.hideKeyboard(OTPActivity.this);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}