package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.DeliveryExpertHomeActivity;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.RestaurtantMainActivity;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateProfileActivity extends AppCompatActivity {
    UpdateProfileActivity context;
    private SavePref savePref;

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.address_layout)
    LinearLayout addressLayout;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.update)
    Button update;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.video_play)
    ImageView videoPlay;
    @BindView(R.id.document_layout)
    LinearLayout documentLayout;

    private String selectedimage = "", licence_image = "";

    boolean is_profile_pic = false;

    Uri fileUri;
    int PLACE_PICKER_REQUEST = 420;
    String latitude = "", longitude = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);


        context = UpdateProfileActivity.this;
        savePref = new SavePref(context);

        if (savePref.getUser_type().equals("2")) {
            addressLayout.setVisibility(View.VISIBLE);
            documentLayout.setVisibility(View.VISIBLE);
            text.setText("Upload Picture of Restaurant Licence");
        } else if (savePref.getUser_type().equals("3")) {
            addressLayout.setVisibility(View.GONE);
            documentLayout.setVisibility(View.VISIBLE);
            text.setText("Upload Picture of School ID or National ID");
        } else {
            addressLayout.setVisibility(View.GONE);
            documentLayout.setVisibility(View.GONE);
        }


        setdata();
    }

    private void setdata() {
        name.setText(savePref.getName());
        email.setText(savePref.getEmail());
        phone.setText(savePref.getStringLatest(Parameters.COUNTRY_CODE) + savePref.getPhone());
        Glide.with(context).load(savePref.getImage()).error(R.drawable.placeholder).into(profilePic);

        if (savePref.getUser_type().equals("3"))
            Glide.with(context).load(savePref.getStringLatest(Parameters.DELIVERYEXPERTIDIMAGE)).error(R.drawable.placeholder).into(image);


        if (savePref.getUser_type().equals("2")) {
            Glide.with(context).load(savePref.getStringLatest(Parameters.RESTAURTANTLICENSEIMAGE)).error(R.drawable.placeholder).into(image);
            address.setText(savePref.getStringLatest(Parameters.ADDRESS));
        }


    }


    @OnClick({R.id.back_button, R.id.image, R.id.profile_pic, R.id.address, R.id.update})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.profile_pic:
                is_profile_pic = true;
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.image:
                is_profile_pic = false;
                ImagePicker.Companion.with(this)
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
                break;
            case R.id.address:
                Intent i = new Intent(this, FetchLocationActivity.class);
                startActivityForResult(i, 2000);
                break;
            case R.id.update:
                EDIT_PROFILEProcess();
                break;
        }
    }

    private void EDIT_PROFILEProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Name");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (email.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(email.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter Vaild Email Address");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                EDIT_PROFILE_API();
            }
        } else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void EDIT_PROFILE_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        String parameter_name = "";
        if (savePref.getUser_type().equals("2"))
            parameter_name = "restaurantLicenseImage";
        else
            parameter_name = "deliveryExpertIdImage";

        if (!licence_image.isEmpty()) {
            final MediaType MEDIA_TYPE = licence_image.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(licence_image);
            formBuilder.addFormDataPart(parameter_name, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, email.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.COUNTRY_CODE, "1");

        if (savePref.getUser_type().equals("2")) {
            formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
            formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);
            formBuilder.addFormDataPart(Parameters.ADDRESS, address.getText().toString());
        }
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setAuthorization_key(body.getString("token"));
                            savePref.setID(body.getString("id"));
                            savePref.setName(body.optString("name"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setImage(body.getString("image"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setUser_type(body.getString("role"));
                            savePref.setStringLatest(Parameters.COUNTRY_CODE, body.optString(Parameters.COUNTRY_CODE));

                            savePref.setStringLatest(Parameters.ADDRESS, body.optString(Parameters.ADDRESS));
                            savePref.setStringLatest(Parameters.LATITUDE, body.optString(Parameters.LATITUDE));
                            savePref.setStringLatest(Parameters.LONGITUDE, body.optString(Parameters.LONGITUDE));
                            savePref.setStringLatest(Parameters.RESTAURTANTLICENSEIMAGE, body.optString(Parameters.RESTAURTANTLICENSEIMAGE));

                            savePref.setStringLatest(Parameters.DELIVERYEXPERTIDIMAGE, body.optString(Parameters.DELIVERYEXPERTIDIMAGE));

                            finish();
                            util.showToast(context, "Profile Updated Successfully!");
                        } else {
                            update.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void place_picker() {
        // Initialize Places.
        Places.initialize(context, "AIzaSyAA-k3KpN8PbS5u4_9qLFGIFZ_fIm52iM4");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(context);
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID,
                Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.ID, Place.Field.PHONE_NUMBER, Place.Field.RATING, Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(context);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                if (is_profile_pic) {
                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(profilePic);
                } else {
                    licence_image = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(licence_image).into(image);
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else if (resultCode == RESULT_OK && requestCode == 2000) {

            address.setText(data.getStringExtra("location"));
            latitude = String.valueOf(data.getStringExtra("latitude"));
            longitude = String.valueOf(data.getStringExtra("longitude"));
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (resultCode == RESULT_OK) {

                    // Get Place data from intent
                    Place place = Autocomplete.getPlaceFromIntent(data);

                    address.setText(place.getName());
                    latitude = String.valueOf(place.getLatLng().latitude);
                    longitude = String.valueOf(place.getLatLng().longitude);

                }
            }
        } else {
            if (resultCode == RESULT_OK) {
                Uri resultUri = data.getData();

                licence_image = getAbsolutePath(this, resultUri);

                Glide.with(this).load(licence_image).into(image);
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
}