package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.ModelClasses.CategoryModel;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.enjeal_nys_food.parser.GetAsyncGet;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddFoodActivity extends AppCompatActivity {

    AddFoodActivity context;
    @BindView(R.id.promotion_price_layout)
    LinearLayout promotionPriceLayout;
    @BindView(R.id.promotion_end_date_layout)
    LinearLayout promotionEndDateLayout;
    @BindView(R.id.quantity)
    EditText quantity;
    @BindView(R.id.promotion_price)
    EditText promotionPrice;
    @BindView(R.id.promotion_end_date)
    TextView promotionEndDate;
    private SavePref savePref;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.food_name)
    EditText foodName;
    @BindView(R.id.price)
    EditText price;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.add_food)
    Button addFood;
    private String selectedimage = "", category_id = "";
    Uri fileUri;
    boolean from_edit = false;
    boolean from_promotion = false;
    private ArrayList<CategoryModel> list;
    MenuItemModel menuItemModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);
        ButterKnife.bind(this);


        context = AddFoodActivity.this;
        savePref = new SavePref(context);

        from_promotion = getIntent().getBooleanExtra("from_promotion", false);
        from_edit = getIntent().getBooleanExtra("from_edit", false);


        if (from_promotion) {
            foodName.setEnabled(false);
            price.setEnabled(false);
            category.setEnabled(false);
            description.setEnabled(false);
            quantity.setEnabled(false);
            image.setEnabled(false);
            addFood.setText("Launch Promotion");
            promotionPriceLayout.setVisibility(View.VISIBLE);
            promotionEndDateLayout.setVisibility(View.VISIBLE);

            menuItemModel = getIntent().getExtras().getParcelable("data");
            category.setText(menuItemModel.getCategory_name());
            foodName.setText(menuItemModel.getName());
            price.setText(menuItemModel.getPrice());

            Glide.with(context).load(menuItemModel.getImage()).into(image);
            description.setText(menuItemModel.getDescription());
            quantity.setText(menuItemModel.getQuantity());
            promotionPrice.setText(menuItemModel.getPromotionPrice());
            if (!menuItemModel.getPromotionDate().isEmpty())
                promotionEndDate.setText(util.convertTimeStampDate(Long.parseLong(menuItemModel.getPromotionDate())));
            category_id = menuItemModel.getCategory_id();
            //  promotionEndDate.addTextChangedListener(tw);

        } else if (from_edit) {
            menuItemModel = getIntent().getExtras().getParcelable("data");

            promotionPriceLayout.setVisibility(View.GONE);
            promotionEndDateLayout.setVisibility(View.GONE);

            category.setText(menuItemModel.getCategory_name());
            foodName.setText(menuItemModel.getName());
            price.setText(menuItemModel.getPrice());

            Glide.with(context).load(menuItemModel.getImage()).into(image);

            description.setText(menuItemModel.getDescription());
            quantity.setText(menuItemModel.getQuantity());

            category_id = menuItemModel.getCategory_id();

            addFood.setText("Update");
        } else {
            promotionPriceLayout.setVisibility(View.GONE);
            promotionEndDateLayout.setVisibility(View.GONE);
        }

        setToolbar();

        if (ConnectivityReceiver.isConnected()) {
            CATEGORIES(false);
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


    }

    private void CATEGORIES(boolean flag) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.FOODCATEGORYLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.getString("image"));
                                list.add(categoryModel);
                            }

                            if (flag) {
                                Show_Category();
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Show_Category() {
        // setup the alert builder
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Choose an Category");

        /*ArrayList to Array Conversion */
        String array[] = new String[list.size()];
        for (int j = 0; j < list.size(); j++) {
            array[j] = list.get(j).getName();


        }


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                category.setText(list.get(which).getName());
                category_id = list.get(which).getId();
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (from_promotion)
            title.setText("Promotion");
        else if (from_edit)
            title.setText("Update Food");
        else
            title.setText("Add Food");


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.image, R.id.promotion_end_date, R.id.category, R.id.add_food})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.promotion_end_date:
                new SingleDateAndTimePickerDialog.Builder(this)
                        .bottomSheet()
                        .title("Promotion End Date")
                        .curved()
                        .mustBeOnFuture()
                        .displayMinutes(false)
                        .displayHours(false)
                        .displayDays(true)
                        .displayMonth(true)
                        .displayYears(true)
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

                                promotionEndDate.setText(dateFormat.format(date));

                            }
                        })
                        .display();
                break;
            case R.id.category:
                if (list != null) {
                    Show_Category();
                } else {
                    if (list == null)
                        list = new ArrayList<>();

                    CATEGORIES(true);
                }
                break;
            case R.id.add_food:
                AddFoodTask();
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(image);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private void AddFoodTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (foodName.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Food Name");
                addFood.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (price.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Food Price");
                addFood.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (category.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Food Category");
                addFood.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (description.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Food Description");
                addFood.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (quantity.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Food Plate Available");
                addFood.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (from_edit) {
                    EDITFOODMENUITEM_API();
                } else if (from_promotion) {
                    if (promotionPrice.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Promotion Price");
                        addFood.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (promotionEndDate.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Select Promotion End Date");
                        addFood.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        EDITFOODMENUITEM_API();
                    }
                } else {
                    if (selectedimage.isEmpty()) {
                        util.IOSDialog(context, "Please Select Image");
                        addFood.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        ADDFOODMENUITEM_API();
                    }
                }

            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void ADDFOODMENUITEM_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.QUANTITY, quantity.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);
        formBuilder.addFormDataPart(Parameters.NAME, foodName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PRICE, price.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADDFOODMENUITEM, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            String t = "";
                            t = "Food Item Added Successfully!";
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(t).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();

    }

    private void EDITFOODMENUITEM_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.ID, menuItemModel.getId());
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);
        formBuilder.addFormDataPart(Parameters.NAME, foodName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PRICE, price.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.QUANTITY, quantity.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().trim());
        if (from_promotion) {
            formBuilder.addFormDataPart(Parameters.PROMOTION_PRICE, promotionPrice.getText().toString().trim());
            formBuilder.addFormDataPart(Parameters.PROMOTION_DATE, util.convertDateTimeStamp(promotionEndDate.getText().toString().trim()));
        }
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.EDITFOODMENUITEM, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            String t = "";
                            if (from_promotion) {
                                t = "Food Promoton Launched Successfully!";
                            } else {
                                t = "Food Item Updated Successfully!";
                            }
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(t).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();

    }

    TextWatcher tw = new TextWatcher() {
        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().equals(current)) {
                String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8) {
                    clean = clean + ddmmyyyy.substring(clean.length());
                } else {
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                    int day = Integer.parseInt(clean.substring(0, 2));
                    int mon = Integer.parseInt(clean.substring(2, 4));
                    int year = Integer.parseInt(clean.substring(4, 8));

                    mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                    cal.set(Calendar.MONTH, mon - 1);
                    year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                    cal.set(Calendar.YEAR, year);
                    // ^ first set year for the line below to work correctly
                    //with leap years - otherwise, date e.g. 29/02/2012
                    //would be automatically corrected to 28/02/2012

                    day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                    clean = String.format("%02d%02d%02d", day, mon, year);
                }

                clean = String.format("%s/%s/%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                current = clean;
                promotionEndDate.setText(current);
                // promotionEndDate.setSelection(sel < current.length() ? sel : current.length());
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };


}