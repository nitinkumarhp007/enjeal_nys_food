package com.enjeal_nys_food.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.enjeal_nys_food.Adapters.RestaurantListAdapter;
import com.enjeal_nys_food.ModelClasses.UserModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.enjeal_nys_food.parser.GetAsyncPut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RestorentListActivity extends AppCompatActivity {
    RestorentListActivity context;
    private SavePref savePref;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    String categoryId = "";
    RestaurantListAdapter listAdapter = null;
    ArrayList<UserModel> list;
    String latitude;
    String longitude;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restorent_list);
        ButterKnife.bind(this);

        context = RestorentListActivity.this;
        savePref = new SavePref(context);

        setToolbar();


        categoryId = getIntent().getStringExtra("category_id");
        latitude = getIntent().getStringExtra("latitude");
        longitude = getIntent().getStringExtra("longitude");

        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (listAdapter != null)
                    listAdapter.filter(s.toString().trim());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected())
            GETRESTAURANTLIST();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void GETRESTAURANTLIST() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, categoryId);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.GETRESTAURANTLIST, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray restarurants = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < restarurants.length(); i++) {
                                JSONObject object = restarurants.getJSONObject(i);
                                UserModel userModel = new UserModel();
                                userModel.setId(object.getString("id"));
                                userModel.setEmail(object.getString("email"));
                                userModel.setImage(object.getString("image"));
                                userModel.setName(object.getString("name"));
                                userModel.setIs_fav(object.optString("isFav"));
                                userModel.setAvgRating(object.optString("avgRating"));
                                userModel.setDistance(object.optString("distance"));
                                userModel.setOnline_status(object.optString("onlineStatus"));
                                list.add(userModel);
                            }
                            if (list.size() > 0) {
                                listAdapter = new RestaurantListAdapter(context, list);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                                myRecyclerView.setAdapter(listAdapter);
                                errorMessage.setVisibility(View.INVISIBLE);
                                myRecyclerView.setVisibility(View.VISIBLE);

                            } else {
                                myRecyclerView.setVisibility(View.INVISIBLE);
                                errorMessage.setVisibility(View.VISIBLE);
                            }

                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void FAV_UNFAV_RESTAURANT(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.RESTAURANTID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.FAV_UNFAV_RESTAURANT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {

                            if (list.get(position).getIs_fav().equals("1")) {
                                list.get(position).setIs_fav("0");
                            } else {
                                list.get(position).setIs_fav("1");
                            }
                            listAdapter.notifyDataSetChanged();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getIntent().getStringExtra("category_name"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}