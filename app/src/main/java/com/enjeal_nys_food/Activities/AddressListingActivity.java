package com.enjeal_nys_food.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enjeal_nys_food.Adapters.AddresslistAdapter;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressListingActivity extends AppCompatActivity {
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    AddressListingActivity context;
    SavePref savePref;
    @BindView(R.id.add_new_address)
    Button addNewAddress;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.place_order)
    Button placeOrder;

    AddresslistAdapter adapter = null;
    String restaurantId = "", foodMenuItemIds = "", price = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_listing);
        ButterKnife.bind(this);

        context = AddressListingActivity.this;
        savePref = new SavePref(context);

        adapter = new AddresslistAdapter(context);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(adapter);
        myRecyclerView.setVisibility(View.VISIBLE);

        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Address List");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }



    @OnClick({R.id.add_new_address, R.id.place_order})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_new_address:
                startActivity(new Intent(this, AddaddressActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.place_order:
                Intent intent = new Intent(context, ChoosePaymentActivity.class);
                //intent.putExtra("id", list.get(position).getId());
                context.startActivity(intent);
                break;
        }
    }
}