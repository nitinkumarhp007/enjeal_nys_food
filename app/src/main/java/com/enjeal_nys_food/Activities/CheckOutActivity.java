package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Adapters.FoodAdapter;
import com.enjeal_nys_food.Adapters.FoodAdapterCheckout;
import com.enjeal_nys_food.Adapters.OrderRequestAdapter;
import com.enjeal_nys_food.Adapters.PrimaryFoodAdapter;
import com.enjeal_nys_food.Fragments.User.Restaurtant.HomeRestorentFragment;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.ModelClasses.MenuItemModel;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.GPSTracker;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.enjeal_nys_food.parser.GetAsyncGet;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CheckOutActivity extends AppCompatActivity {

    CheckOutActivity context;
    @BindView(R.id.info_text)
    TextView infoText;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_phone)
    TextView userPhone;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.expected_time_of_delivery)
    TextView expectedTimeOfDelivery;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.status_button)
    Button statusButton;
    @BindView(R.id.items_total)
    TextView items_total;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.address_layout)
    LinearLayout address_layout;
    @BindView(R.id.commission)
    TextView commission;
    @BindView(R.id.final_total_pay)
    TextView final_total_pay;
    @BindView(R.id.delivery_charges)
    TextView delivery_charges;
    @BindView(R.id.specificity_food)
    EditText specificity_food;
    @BindView(R.id.estimated_time)
    TextView estimated_time;

    ArrayList<MenuItemModel> list_selected;

    String matrix_url = "", time = "", latitude = "", longitude = "", latitude_res = "", longitude_res = "", restaurtant_id = "", PRICE = "";
    GPSTracker gpsTracker = null;

    double total_cost = 0.0;
    double t_cost = 0.0;
    double delivery_cost = 0.0;
    double t_commission = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        ButterKnife.bind(this);

        context = CheckOutActivity.this;
        savePref = new SavePref(context);

        gpsTracker = new GPSTracker(context);


        userName.setText(getIntent().getStringExtra("name"));
        restaurtant_id = getIntent().getStringExtra("restaurtant_id");
        userPhone.setText(getIntent().getStringExtra("phone"));
        items_total.setText("Price: XAF" + getIntent().getStringExtra("total"));
        Glide.with(context).load(getIntent().getStringExtra("image_text")).into(image);

        list_selected = getIntent().getParcelableArrayListExtra("list_selected");
        PRICE = getIntent().getStringExtra("total");
        latitude_res = getIntent().getStringExtra("latitude_res");
        longitude_res = getIntent().getStringExtra("longitude_res");

        time = getIntent().getStringExtra("time_d");


        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new FoodAdapterCheckout(context, list_selected));


        if (!gpsTracker.canGetLocation()) {
            gpsTracker.showSettingsAlert();
        } else {
            address.setText(util.getCompleteAddressString(context, gpsTracker.getLatitude(), gpsTracker.getLongitude()));

            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());


            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Log.e("dataaa", "address_ " + address_);
                Log.e("dataaa", "city " + city);
                Log.e("dataaa", "state " + state);
                Log.e("dataaa", "country " + country);
                Log.e("dataaa", "postalCode " + postalCode);
                Log.e("dataaa", "knownName " + knownName);

                Log.e("dataaa", "subadminaera " + addresses.get(0).getSubAdminArea());
                Log.e("dataaa", "subalocality " + addresses.get(0).getSubLocality());


                address.setText(address_);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //kharar : 30.7499 76.6411
        //Chandigrah : 30.7333 76.7794


        matrix_url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + latitude + "," + longitude +
                "&destinations=" + latitude_res + "," + longitude_res + "&key=" + getResources().getString(R.string.APISERVERKEY);


        if (ConnectivityReceiver.isConnected())
            GetDistanceAPI();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


        setToolbar();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Check Out");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @OnClick({R.id.address_layout, R.id.expected_time_of_delivery, R.id.status_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address_layout:
                Intent i = new Intent(this, FetchLocationActivity.class);
                startActivityForResult(i, 2000);
                break;
            case R.id.expected_time_of_delivery:
                new SingleDateAndTimePickerDialog.Builder(this)
                        .bottomSheet()
                        .curved()
                        .displayAmPm(false)
                        .mustBeOnFuture()
                        .displayMinutes(true)
                        .displayHours(true)
                        .displayDays(false)
                        .displayMonth(false)
                        .displayYears(false)
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                                expectedTimeOfDelivery.setText(dateFormat.format(date));

                            }
                        })
                        .display();

                break;
            case R.id.status_button:
                if (ConnectivityReceiver.isConnected()) {

                    String item_selected = "";
                    ArrayList<MenuItemModel> selected_list_new = new ArrayList<>();
                    for (int j = 0; j < list_selected.size(); j++) {
                        if (list_selected.get(j).isIs_select()) {
                            if (item_selected.isEmpty()) {
                                item_selected = list_selected.get(j).getId();
                            } else {
                                item_selected = item_selected + "," + list_selected.get(j).getId();
                            }
                            selected_list_new.add(list_selected.get(j));
                        }
                    }
                    ADDFOODMENUITEM_API(item_selected);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    private void ADDFOODMENUITEM_API(String foodMenuItemIds) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.RESTAURANTID, restaurtant_id);
        formBuilder.addFormDataPart(Parameters.PRICE, String.valueOf(total_cost));

        formBuilder.addFormDataPart(Parameters.INITIALCOST, String.valueOf(t_cost));
        formBuilder.addFormDataPart(Parameters.COMMSIION, String.valueOf(t_commission));
        formBuilder.addFormDataPart(Parameters.DELIVERYCHARGES, String.valueOf(delivery_cost));

        formBuilder.addFormDataPart(Parameters.FOODMENUITEMIDS, foodMenuItemIds);
        formBuilder.addFormDataPart(Parameters.LOCATION, address.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
        formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);
        formBuilder.addFormDataPart(Parameters.DELIVERYTYPE, "0");
        //formBuilder.addFormDataPart(Parameters.EXPECTEDTIMEDELIVERY, expectedTimeOfDelivery.getText().toString().trim().trim());//15:00 //HH:MM
        formBuilder.addFormDataPart(Parameters.EXPECTEDTIMEDELIVERY, expectedTimeOfDelivery.getText().toString());//15:00 //HH:MM
        formBuilder.addFormDataPart(Parameters.SPECIALITYONFOODORDER, specificity_food.getText().toString().trim());//15 Mins
        formBuilder.addFormDataPart(Parameters.ESTIMATEDDELIVERYTIME, String.valueOf(time));
        //formBuilder.addFormDataPart(Parameters.ESTIMATEDDELIVERYTIME, "12");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADDRESTAURTANTORDER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            String t = "";
                            t = "Food order request has been sent to restaurant,Restaurant will respond soon!";
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(t).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.putExtra("from_request", true);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();

    }

    private void GetDistanceAPI() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        try {
            if (mDialog != null)
                mDialog.show();
        } catch (Exception e) {
        }

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FOODCATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, matrix_url, formBody) {
            @Override
            public void getValueParse(String result) {
                Log.e("resulttttt", result);
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        //JSONArray body = jsonmainObject.getJSONArray("body");

                        String time_string = jsonmainObject.getJSONArray("rows").getJSONObject(0)
                                .getJSONArray("elements").getJSONObject(0).
                                        getJSONObject("duration").getString("text");

                        estimated_time.setText("Estimated Delivery Time from restaurants to your actual location: " + time_string);


                        String distance_text = jsonmainObject.getJSONArray("rows").getJSONObject(0)
                                .getJSONArray("elements").getJSONObject(0).getJSONObject("distance").getString("text");


                        time = time_string.substring(0, time_string.lastIndexOf(" "));


                        GETPRICESAPI(mDialog, distance_text.substring(0, distance_text.lastIndexOf(" ")));


                        Log.e("resulttttt", time);


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void GETPRICESAPI(ProgressDialog mDialog, String distance_text) {
        Log.e("resulttttt", "distance_text:" + distance_text);
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FOODCATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.GETPRICES, formBody) {
            @Override
            public void getValueParse(String result) {
                try {
                    if (mDialog != null)
                        mDialog.dismiss();
                } catch (Exception e) {
                }

                Log.e("resulttttt", result);
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject body = jsonmainObject.getJSONObject("body");

                        String delivery_cost_api_upto_1 = body.getString("deliveryChargesUpto1km");
                        String delivery_cost_api_upto_2 = body.getString("deliveryChargesUpto2km");
                        String delivery_cost_api_upto_5 = body.getString("deliveryChargesUpto5km");
                        String delivery_cost_api_more_then_5 = body.getString("deliveryChargesMoreThan5km");
                        String commission_percentage = body.getString("commissionPercentage");

                        if (Double.parseDouble(distance_text) > 5)
                            delivery_cost = Double.parseDouble(delivery_cost_api_more_then_5);
                        if (Double.parseDouble(distance_text) <= 5 && Double.parseDouble(distance_text) > 2)
                            delivery_cost = Double.parseDouble(delivery_cost_api_upto_5);
                        if (Double.parseDouble(distance_text) > 1 && Double.parseDouble(distance_text) <= 2)
                            delivery_cost = Double.parseDouble(delivery_cost_api_upto_2);
                        if (Double.parseDouble(distance_text) <= 1)
                            delivery_cost = Double.parseDouble(delivery_cost_api_upto_1);


                        Log.e("delivery_cost_dis___", distance_text + "---" + String.valueOf(delivery_cost));


                        t_cost = Double.parseDouble(PRICE);

                        t_commission = (t_cost * Double.parseDouble(commission_percentage)) / 100;

                        total_cost = t_cost + t_commission + delivery_cost;

                        commission.setText("Commission: XAF" + t_commission);
                        delivery_charges.setText("Delivery Charges: XAF" + delivery_cost);
                        final_total_pay.setText("Total Payable: XAF" + total_cost);


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 2000) {
                address.setText(data.getStringExtra("location"));
                latitude = String.valueOf(data.getStringExtra("latitude"));
                longitude = String.valueOf(data.getStringExtra("longitude"));

                if (ConnectivityReceiver.isConnected())
                    GetDistanceAPI();
                else
                    util.IOSDialog(context, util.internet_Connection_Error);
            }
        }
    }
}