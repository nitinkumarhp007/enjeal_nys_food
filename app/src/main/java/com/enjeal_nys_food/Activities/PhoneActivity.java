package com.enjeal_nys_food.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PhoneActivity extends AppCompatActivity {

    PhoneActivity context;
    @BindView(R.id.next)
    Button next;
    @BindView(R.id.country)
    TextView country;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.back_button)
    ImageView back_button;

    String user_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        ButterKnife.bind(this);

        context = PhoneActivity.this;
        user_type = getIntent().getStringExtra("user_type");


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    if (country.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Select Country");
                        next.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                    } else if (phone.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Phone");
                        next.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                    } else {
                        PHONEVERIFY_API();
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        });

        country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountryPicker.Builder builder =
                        new CountryPicker.Builder().with(context)
                                .listener(new OnCountryPickerListener() {
                                    @Override
                                    public void onSelectCountry(Country c) {
                                        country.setText(c.getDialCode());
                                    }
                                }).sortBy(CountryPicker.SORT_BY_NAME).theme(CountryPicker.THEME_OLD);

                CountryPicker picker = builder.build();
                picker.showDialog(context);
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void PHONEVERIFY_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.COUNTRY_CODE, country.getText().toString().trim().substring(1));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.PHONEVERIFY, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            Intent intent = new Intent(context, OTPActivity.class);
                            intent.putExtra("authorization_key", jsonMainobject.getJSONObject("body").optString("token"));
                            intent.putExtra("otp", jsonMainobject.getJSONObject("body").getString("otp"));
                            intent.putExtra("user_type", user_type);
                            intent.putExtra("phone", phone.getText().toString().trim());
                            intent.putExtra("country", country.getText().toString().trim().substring(1));
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}