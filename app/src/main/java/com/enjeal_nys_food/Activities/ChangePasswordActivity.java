package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncPut;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChangePasswordActivity extends AppCompatActivity {
    ChangePasswordActivity context;
    private SavePref savePref;
    @BindView(R.id.current_password)
    EditText currentPassword;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.change)
    Button change;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        context = ChangePasswordActivity.this;
        savePref = new SavePref(context);

        setToolbar();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Change Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @OnClick(R.id.change)
    public void onViewClicked() {
        if (ConnectivityReceiver.isConnected()) {
            if (currentPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter Current Password");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (newPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter New Password");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (confirmPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter Confirm Password");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                util.IOSDialog(context, "Password not match");
                change.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                CHANGE_PASSWORD_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void CHANGE_PASSWORD_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OLD_PASSWORD, currentPassword.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.NEW_PASSWORD, newPassword.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.CHANGEPASSWORD, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonMainobject.getString("message"));
                            finish();
                            util.hideKeyboard(context);
                        } else {
                            if (jsonMainobject.getString("message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}