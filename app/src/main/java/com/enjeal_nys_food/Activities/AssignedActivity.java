package com.enjeal_nys_food.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class AssignedActivity extends AppCompatActivity {
    AssignedActivity context;
    @BindView(R.id.info_text)
    TextView infoText;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_phone)
    TextView userPhone;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.info_text_delivery_boy)
    TextView infoTextDeliveryBoy;
    @BindView(R.id.user_name_delivery_boy)
    TextView userNameDeliveryBoy;
    @BindView(R.id.user_phone_delivery_boy)
    TextView userPhoneDeliveryBoy;
    @BindView(R.id.image_delivery_boy)
    CircleImageView imageDeliveryBoy;
    @BindView(R.id.back_to_home)
    Button backToHome;
    @BindView(R.id.evaluation_restaurant)
    TextView evaluationRestaurant;
    @BindView(R.id.delivery_expert_lay)
    LinearLayout delivery_expert_lay;
    @BindView(R.id.evaluation_delivery)
    TextView evaluationDelivery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned);
        ButterKnife.bind(this);

        context = AssignedActivity.this;
        setToolbar();

        userName.setText(getIntent().getStringExtra("name_r"));
        // userPhone.setText(getIntent().getStringExtra("name_r"));

        Glide.with(context).load(getIntent().getStringExtra("image_r")).into(image);


        if (getIntent().getStringExtra("deliveryBy").equals("1")) {
            delivery_expert_lay.setVisibility(View.INVISIBLE);
        } else {
            userNameDeliveryBoy.setText(getIntent().getStringExtra("name_d"));
            userPhoneDeliveryBoy.setText(getIntent().getStringExtra("phone_d"));
            Glide.with(context).load(getIntent().getStringExtra("image_d")).into(imageDeliveryBoy);
        }


    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Order Info");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

    @OnClick(R.id.back_to_home)
    public void onClick() {
        onBackPressed();
    }

    @OnClick({R.id.evaluation_restaurant, R.id.evaluation_delivery})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.evaluation_restaurant:
                break;
            case R.id.evaluation_delivery:
                break;
        }
        startActivity(new Intent(context, PostRatingActivity.class));
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}