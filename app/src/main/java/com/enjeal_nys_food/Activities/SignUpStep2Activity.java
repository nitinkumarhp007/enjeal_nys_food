package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpStep2Activity extends AppCompatActivity {
    SignUpStep2Activity context;
    @BindView(R.id.back_button)
    ImageView backButton;
    private SavePref savePref;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.video_play)
    ImageView videoPlay;
    @BindView(R.id.sign_up)
    Button signUp;

    String user_type = "";

    private String licence_image = "";
    Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_step2);
        ButterKnife.bind(this);

        context = SignUpStep2Activity.this;
        savePref = new SavePref(context);

        user_type = getIntent().getStringExtra("user_type");

        if (user_type.equals("2")) {
            text.setText("Upload Picture of Restaurant Licence");
        } else {
            text.setText("Upload Picture of School ID or National ID");
        }

    }


    @OnClick({R.id.back_button, R.id.image, R.id.sign_up})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.image:
                //  CropImage.activity(fileUri).start(this);
                ImagePicker.Companion.with(this)
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
                break;
            case R.id.sign_up:
                if (ConnectivityReceiver.isConnected()) {
                    if (licence_image.trim().isEmpty()) {
                        if (user_type.equals("2")) {
                            util.IOSDialog(context, "Please Select Restaurant Licence Picture");
                        } else {
                            util.IOSDialog(context, "Please Select School ID or National ID Picture");
                        }
                        signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        USER_SIGNUP_API();
                    }
                } else
                    util.IOSDialog(context, util.internet_Connection_Error);
                break;
        }
    }

    private void USER_SIGNUP_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!getIntent().getStringExtra("selected_image").isEmpty()) {
            final MediaType MEDIA_TYPE = getIntent().getStringExtra("selected_image").endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(getIntent().getStringExtra("selected_image"));
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        String parameter_name = "";
        if (user_type.equals("2"))
            parameter_name = "restaurantLicenseImage";
        else
            parameter_name = "deliveryExpertIdImage";

        if (!licence_image.isEmpty()) {
            final MediaType MEDIA_TYPE = licence_image.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(licence_image);
            formBuilder.addFormDataPart(parameter_name, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.NAME, getIntent().getStringExtra("name"));
        formBuilder.addFormDataPart(Parameters.EMAIL, getIntent().getStringExtra("email"));
        formBuilder.addFormDataPart(Parameters.PASSWORD, getIntent().getStringExtra("password"));
        formBuilder.addFormDataPart(Parameters.PHONE, getIntent().getStringExtra("phone"));
        formBuilder.addFormDataPart(Parameters.COUNTRY_CODE, getIntent().getStringExtra("country_code"));
        formBuilder.addFormDataPart(Parameters.ROLE, user_type);

        if (user_type.equals("2")) {
            formBuilder.addFormDataPart(Parameters.LATITUDE, getIntent().getStringExtra("latitude"));
            formBuilder.addFormDataPart(Parameters.LONGITUDE, getIntent().getStringExtra("longitude"));
            formBuilder.addFormDataPart(Parameters.ADDRESS, getIntent().getStringExtra("address"));
        }
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "0");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);

                            util.hideKeyboard(context);
                            Intent intent = new Intent(context, OTPActivity.class);
                            intent.putExtra("authorization_key", jsonMainobject.getJSONObject("body").getString("token"));
                            intent.putExtra("user_type", user_type);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                        } else {
                            signUp.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                licence_image = getAbsolutePath(this, resultUri);

                Glide.with(this).load(licence_image).into(image);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else {
            if (resultCode == RESULT_OK) {
                Uri resultUri = data.getData();

                licence_image = getAbsolutePath(this, resultUri);

                Glide.with(this).load(licence_image).into(image);
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
}