package com.enjeal_nys_food.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.enjeal_nys_food.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseSignUpAsActivity extends AppCompatActivity {

    @BindView(R.id.user)
    Button user;
    @BindView(R.id.restorent_owner)
    Button restorentOwner;
    @BindView(R.id.delivery_expert)
    Button deliveryExpert;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_sign_up_as);
        ButterKnife.bind(this);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @OnClick({R.id.user, R.id.restorent_owner, R.id.delivery_expert})
    public void onClick(View view) {
        Intent intent = new Intent(this, SignUpActivity.class);
        switch (view.getId()) {
            case R.id.user:
                intent.putExtra("user_type", "1");
                startActivity(intent);
                break;
            case R.id.restorent_owner:
                intent.putExtra("user_type", "2");
                startActivity(intent);
                break;
            case R.id.delivery_expert:
                intent.putExtra("user_type", "3");
                startActivity(intent);
                break;

        }
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}