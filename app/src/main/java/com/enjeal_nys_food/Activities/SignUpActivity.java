package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.GPSTracker;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpActivity extends AppCompatActivity {
    SignUpActivity context;
    @BindView(R.id.sign_up_text)
    TextView signUpText;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.accept_the_terms_amp_conditions)
    CheckBox acceptTheTermsAmpConditions;
    @BindView(R.id.sign_up)
    Button signUp;
    @BindView(R.id.sign_in)
    Button signIn;

    String user_type = "";
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.country)
    TextView country;
    @BindView(R.id.text_terms_amp_conditions)
    TextView text_terms_amp_conditions;
    @BindView(R.id.address_layout)
    LinearLayout addressLayout;
    private String selectedimage = "";
    Uri fileUri;
    int PLACE_PICKER_REQUEST = 420;
    int ADDRESS_PICKER_REQUEST = 4200;
    String latitude = "", longitude = "";

    GPSTracker gpsTracker = null;

    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        context = SignUpActivity.this;
        savePref = new SavePref(context);
        gpsTracker = new GPSTracker(context);

        user_type = getIntent().getStringExtra("user_type");


        signUp.setText("Next");

        if (user_type.equals("1")) {
            signUpText.setText("Client Signup");
            addressLayout.setVisibility(View.GONE);
        } else if (user_type.equals("2")) {
            signUpText.setText("Restaurant Signup");
            signUp.setText("Next");
            addressLayout.setVisibility(View.VISIBLE);

            MapUtility.apiKey = getResources().getString(R.string.APISERVERKEY);

            if (!gpsTracker.canGetLocation()) {
                gpsTracker.showSettingsAlert();
            } else {
                address.setText(util.getCompleteAddressString(context, gpsTracker.getLatitude(), gpsTracker.getLongitude()));

                latitude = String.valueOf(gpsTracker.getLatitude());
                longitude = String.valueOf(gpsTracker.getLongitude());


                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    Log.e("dataaa", "address_ " + address_);
                    Log.e("dataaa", "city " + city);
                    Log.e("dataaa", "state " + state);
                    Log.e("dataaa", "country " + country);
                    Log.e("dataaa", "postalCode " + postalCode);
                    Log.e("dataaa", "knownName " + knownName);

                    Log.e("dataaa", "subadminaera " + addresses.get(0).getSubAdminArea());
                    Log.e("dataaa", "subalocality " + addresses.get(0).getSubLocality());


                    address.setText(address_);


                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

        } else if (user_type.equals("3")) {
            addressLayout.setVisibility(View.VISIBLE);
            signUpText.setText("Delivery Expert Signup");
            signUp.setText("Next");
        }
    }

    @OnClick({R.id.back_button, R.id.text_terms_amp_conditions, R.id.country, R.id.profile_pic, R.id.address, R.id.sign_up, R.id.sign_in})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.country:
                CountryPicker.Builder builder =
                        new CountryPicker.Builder().with(this)
                                .listener(new OnCountryPickerListener() {
                                    @Override
                                    public void onSelectCountry(Country c) {
                                        country.setText(c.getDialCode());
                                    }
                                }).sortBy(CountryPicker.SORT_BY_NAME).theme(CountryPicker.THEME_OLD);

                CountryPicker picker = builder.build();
                picker.showDialog(this);

                break;
            case R.id.text_terms_amp_conditions:
                Intent intent111 = new Intent(context, TermConditionActivity.class);
                intent111.putExtra("type", "term");
                startActivity(intent111);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.back_button:
                finish();
                break;
            case R.id.profile_pic:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.address:
                // place_picker();
                Intent i = new Intent(this, FetchLocationActivity.class);
                startActivityForResult(i, 2000);
                break;
            case R.id.sign_up:
                SignUpProcess();
                break;
            case R.id.sign_in:
                startActivity(new Intent(this, SignInActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void place_picker() {
        // Initialize Places.
        Places.initialize(context, "AIzaSyAA-k3KpN8PbS5u4_9qLFGIFZ_fIm52iM4");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(context);
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID,
                Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.ID, Place.Field.PHONE_NUMBER, Place.Field.RATING, Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(context);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);
    }

    private void SignUpProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (selectedimage.trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Profile Picture");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Name");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (email.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(email.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter Vaild Email Address");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (country.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Country");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (confirmPassword.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Confirm Password");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
                util.IOSDialog(context, "Password Not Match");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (user_type.equals("1")) {
                    if (!acceptTheTermsAmpConditions.isChecked()) {
                        util.IOSDialog(context, "Please accept the terms and conditions");
                        signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else
                        USER_SIGNUP_API();
                } else if (user_type.equals("2")) {
                    if (address.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Address");
                        signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (!acceptTheTermsAmpConditions.isChecked()) {
                        util.IOSDialog(context, "Please accept the terms and conditions");
                        signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        Intent intent = new Intent(this, SignUpStep2Activity.class);
                        intent.putExtra("user_type", user_type);
                        intent.putExtra("selected_image", selectedimage.trim());
                        intent.putExtra("name", name.getText().toString().trim());
                        intent.putExtra("email", email.getText().toString().trim());
                        intent.putExtra("address", address.getText().toString().trim());
                        intent.putExtra("latitude", latitude);
                        intent.putExtra("longitude", longitude);
                        intent.putExtra("phone", phone.getText().toString().trim());
                        intent.putExtra("password", password.getText().toString().trim());
                        intent.putExtra("country_code", country.getText().toString().trim().substring(1));
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                } else if (user_type.equals("3")) {
                    if (!acceptTheTermsAmpConditions.isChecked()) {
                        util.IOSDialog(context, "Please accept the terms and conditions");
                        signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        Intent intent = new Intent(this, SignUpStep2Activity.class);
                        intent.putExtra("user_type", user_type);
                        intent.putExtra("selected_image", selectedimage.trim());
                        intent.putExtra("name", name.getText().toString().trim());
                        intent.putExtra("email", email.getText().toString().trim());
                        intent.putExtra("phone", phone.getText().toString().trim());
                        intent.putExtra("password", password.getText().toString().trim());
                        intent.putExtra("country_code", country.getText().toString().trim().substring(1));
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }

                }
            }
        } else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void USER_SIGNUP_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, email.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.COUNTRY_CODE, country.getText().toString().trim().substring(1));
        formBuilder.addFormDataPart(Parameters.ROLE, user_type);//1 => user, 2 => restaurant 3=>deliveryExpert
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "0");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            Intent intent = new Intent(context, OTPActivity.class);
                            intent.putExtra("authorization_key", jsonMainobject.getJSONObject("body").getString("token"));
                            intent.putExtra("user_type", user_type);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            //util.showToast(context, "User Registered Successfully!!!");




                        } else {
                            signUp.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                selectedimage = getAbsolutePath(this, resultUri);

                Glide.with(this).load(selectedimage).into(profilePic);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else if (resultCode == RESULT_OK && requestCode == 2000) {

            address.setText(data.getStringExtra("location"));
            latitude = String.valueOf(data.getStringExtra("latitude"));
            longitude = String.valueOf(data.getStringExtra("longitude"));
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (resultCode == RESULT_OK) {

                    // Get Place data from intent
                    Place place = Autocomplete.getPlaceFromIntent(data);

                    //address.setText(place.getName());
                    latitude = String.valueOf(place.getLatLng().latitude);
                    longitude = String.valueOf(place.getLatLng().longitude);


                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(this, Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String address_ = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();


                       /* address.setText(address_ + " " + city + " " + state + " "
                                + country + " " + postalCode + " " + knownName);*/


                        address.setText(address_);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            }
        } else if (requestCode == ADDRESS_PICKER_REQUEST && resultCode == RESULT_OK) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    // String address = data.getStringExtra(MapUtility.ADDRESS);
                    latitude = String.valueOf(data.getDoubleExtra(MapUtility.LATITUDE, 0.0));
                    longitude = String.valueOf(data.getDoubleExtra(MapUtility.LONGITUDE, 0.0));
                    Bundle completeAddress = data.getBundleExtra("fullAddress");
                    /* data in completeAddress bundle
                    "fulladdress"
                    "city"
                    "state"
                    "postalcode"
                    "country"
                    "addressline1"
                    "addressline2"
                     */

                    String addresssss = completeAddress.getString("addressline2") + " " + completeAddress.getString("city")
                            + " " + completeAddress.getString("postalcode") + " " + completeAddress.getString("state")
                            + " " + completeAddress.getString("country");
                    /*address.setText(new StringBuilder().append("addressline2: ").append
                            (completeAddress.getString("addressline2")).append("\ncity: ").append
                            (completeAddress.getString("city")).append("\npostalcode: ").append
                            (completeAddress.getString("postalcode")).append("\nstate: ").append
                            (completeAddress.getString("state")).toString());
*/
                    address.setText(addresssss);
                    /*txtLatLong.setText(new StringBuilder().append("Lat:").append(currentLatitude).append
                            ("  Long:").append(currentLongitude).toString());*/


                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
}