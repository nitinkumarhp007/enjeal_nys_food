package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enjeal_nys_food.Adapters.FoodAdapterCheckout;
import com.enjeal_nys_food.Adapters.PrimaryFoodAdapter;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OrderDetailActivity extends AppCompatActivity {
    OrderDetailActivity context;
    @BindView(R.id.info_text)
    TextView infoText;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_phone)
    TextView userPhone;
    @BindView(R.id.qr_code)
    ImageView qr_code;
    @BindView(R.id.accept)
    TextView accept;
    @BindView(R.id.status)
    TextView status_textview;
    @BindView(R.id.decline)
    TextView decline;

    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.commission)
    TextView commission;
    @BindView(R.id.delivery_charges)
    TextView delivery_charges;
    @BindView(R.id.total_amount_to_pay)
    TextView total_amount_to_pay;
    @BindView(R.id.payment_method)
    TextView payment_method;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.specificity_food)
    TextView specificity_food;
    @BindView(R.id.specificity_food_layout)
    LinearLayout specificity_food_layout;
    @BindView(R.id.expected_time_of_delivery_layout)
    LinearLayout expected_time_of_delivery_layout;
    @BindView(R.id.expected_time_of_delivery)
    TextView expectedTimeOfDelivery;

    @BindView(R.id.accept_decline_layout)
    LinearLayout acceptDeclineLayout;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.status_button)
    Button statusButton;
    @BindView(R.id.estimated_time)
    TextView estimated_time;


    @BindView(R.id.delivery_detail)
    TextView delivery_detail;
    @BindView(R.id.delivery_detail_lay)
    CardView delivery_detail_lay;
    @BindView(R.id.deliveryBy)
    TextView deliveryBy;
    @BindView(R.id.preparationTime)
    TextView preparationTime;

    @BindView(R.id.delivery_expert_info)
    TextView delivery_expert_info;
    @BindView(R.id.delivery_expert_name)
    TextView delivery_expert_name;
    @BindView(R.id.delivery_expert_phone)
    TextView delivery_expert_phone;
    @BindView(R.id.delivery_expert_image)
    ImageView delivery_expert_image;
    @BindView(R.id.delivery_expert_lay)
    CardView delivery_expert_lay;

    RequestModel requestModel;
    double total_time = 0.0;
    boolean from_restaurant = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);

        context = OrderDetailActivity.this;
        savePref = new SavePref(context);

        from_restaurant = getIntent().getBooleanExtra("from_restaurant", false);
        requestModel = getIntent().getExtras().getParcelable("data");

        setToolbar();
        setdata();


    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Order Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.accept, R.id.decline, R.id.status_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.accept:
                show_dialog();
                break;
            case R.id.decline:
                decline_show_dialog();
                break;
            case R.id.status_button:
                Intent intent = new Intent(this, ChoosePaymentActivity.class);
                intent.putExtra("data", requestModel);
                intent.putExtra("total_time", String.valueOf(total_time));
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void show_dialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog);

        TextView submit = (TextView) dialog.findViewById(R.id.submit);
        EditText preparation_time_in_mins = (EditText) dialog.findViewById(R.id.preparation_time_in_mins);
        RadioButton delivery_expert = (RadioButton) dialog.findViewById(R.id.delivery_expert);
        RadioButton restaurant_delivery = (RadioButton) dialog.findViewById(R.id.restaurant_delivery);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    if (preparation_time_in_mins.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Preparation Time (in mins)");
                    } else {
                        String delivery_by = "";

                        if (restaurant_delivery.isChecked())
                            delivery_by = "1";
                        else
                            delivery_by = "2";

                        dialog.dismiss();

                        ACCEPT_DECLINE_RESTAURANT_ORDER(requestModel.getId(), "1", preparation_time_in_mins.getText().toString().trim(), delivery_by, "");
                    }

                } else
                    util.IOSDialog(context, util.internet_Connection_Error);
            }
        });

        dialog.show();
    }

    private void decline_show_dialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_decline);

        TextView submit = (TextView) dialog.findViewById(R.id.submit);
        EditText reason = (EditText) dialog.findViewById(R.id.reason);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    if (reason.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Reason");
                    } else {
                        dialog.dismiss();
                        ACCEPT_DECLINE_RESTAURANT_ORDER(requestModel.getId(), "2", "", "1", reason.getText().toString());

                    }

                } else
                    util.IOSDialog(context, util.internet_Connection_Error);
            }
        });

        dialog.show();
    }

    public void ACCEPT_DECLINE_RESTAURANT_ORDER(String request_id, String status, String preparation_time, String delivery_by, String reason) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, request_id);
        formBuilder.addFormDataPart(Parameters.STATUS, status);//1 => accept, 2 => decline
        formBuilder.addFormDataPart(Parameters.REASON, reason);
        formBuilder.addFormDataPart(Parameters.PREPARATION_TIME, preparation_time);
        formBuilder.addFormDataPart(Parameters.DELIVERYBY, delivery_by);//1 => restaurtant, 2 => delivery boy
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ACCEPT_DECLINE_RESTAURANT_ORDER, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String s = "", s2;
                        if (status.equals("1")) {
                            s = "Request Accepted Successfully";
                            s2 = "Accepted";
                        } else {
                            s = "Request Declined Successfully";
                            s2 = "Declined";
                        }


                        new IOSDialog.Builder(context)
                                .setCancelable(false)
                                .setMessage(s).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                status_textview.setVisibility(View.VISIBLE);
                                accept.setVisibility(View.GONE);
                                decline.setVisibility(View.GONE);

                                status_textview.setText(s2);

                                dialog.dismiss();
                            }
                        }).show();
                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setdata() {
        userName.setText(requestModel.getUsername());
        Glide.with(context).load(requestModel.getImage()).error(R.drawable.place_holder).into(image);

        price.setText("Price: XAF" + requestModel.getInitialcost());
        commission.setText("Commission: XAF" + requestModel.getCommission());
        delivery_charges.setText("Delivery Charges: XAF" + requestModel.getDeliverycharges());
        total_amount_to_pay.setText("Total Payable: XAF" + requestModel.getPrice());

        address.setText(requestModel.getLocation());


        if (savePref.getUser_type().equals("1")) {

            if (requestModel.getStatus().equals("2")) {
                estimated_time.setVisibility(View.VISIBLE);
                estimated_time.setText("Reason: " + requestModel.getReason());
            } else {
                Log.e("dataqqqq", requestModel.getEstimatedDeliveryTime());
                Log.e("dataqqqq", requestModel.getPreparationTime());

                total_time = Double.parseDouble(requestModel.getEstimatedDeliveryTime())
                        + Double.parseDouble(requestModel.getPreparationTime());

                estimated_time.setText("Estimated Delivery Time: " + util.minuteToTime(total_time));
                estimated_time.setVisibility(View.VISIBLE);
            }


        } else {
            estimated_time.setVisibility(View.GONE);

            if (requestModel.getStatus().equals("2")) {
                estimated_time.setVisibility(View.VISIBLE);
                estimated_time.setText("Reason: " + requestModel.getReason());
            }

        }

        if (requestModel.getSpecialRequest().isEmpty())
            specificity_food_layout.setVisibility(View.GONE);
        else {
            specificity_food_layout.setVisibility(View.VISIBLE);
            specificity_food.setText(requestModel.getSpecialRequest());
        }


        if (requestModel.getExpectedTimeDelivery().isEmpty())
            expected_time_of_delivery_layout.setVisibility(View.GONE);
        else {
            expected_time_of_delivery_layout.setVisibility(View.VISIBLE);
            expectedTimeOfDelivery.setText(requestModel.getExpectedTimeDelivery());
        }
        // 0=>pending 1=>accept 2=>decline 3=>paymentPlacedAndDeliveryBoyAssigned 4=>deliveryBoyPickedUp 5=>completed
        if (requestModel.getStatus().equals("0") || requestModel.getStatus().equals("2") || requestModel.getStatus().equals("1")) {
            payment_method.setVisibility(View.GONE);
        } else {
            payment_method.setVisibility(View.VISIBLE);
        }

        if (requestModel.getStatus().equals("3") || requestModel.getStatus().equals("4")) {

            delivery_detail_lay.setVisibility(View.VISIBLE);
            delivery_detail.setVisibility(View.VISIBLE);

            if (requestModel.getDeliveryBy().equals("2")) {
                delivery_expert_info.setVisibility(View.VISIBLE);
                delivery_expert_lay.setVisibility(View.VISIBLE);

                delivery_expert_name.setText(requestModel.getName_d());
                delivery_expert_phone.setText(requestModel.getPhone_d());
                Glide.with(context).load(requestModel.getImage_d()).into(delivery_expert_image);

            } else {
                delivery_expert_info.setVisibility(View.GONE);
                delivery_expert_lay.setVisibility(View.GONE);
            }
        } else {

            delivery_detail_lay.setVisibility(View.GONE);
            delivery_detail.setVisibility(View.GONE);

            delivery_expert_info.setVisibility(View.GONE);
            delivery_expert_lay.setVisibility(View.GONE);
        }


        if (from_restaurant) {

            infoText.setText("Client Info: ");
            userPhone.setText("Phone: " + requestModel.getAddress_phone());


            statusButton.setVisibility(View.GONE);
            qr_code.setVisibility(View.GONE);

            if (requestModel.getStatus().equals("0")) {
                status_textview.setVisibility(View.GONE);
                accept.setVisibility(View.VISIBLE);
                decline.setVisibility(View.VISIBLE);
                acceptDeclineLayout.setVisibility(View.VISIBLE);
            } else if (requestModel.getStatus().equals("1")) {
                status_textview.setVisibility(View.VISIBLE);
                accept.setVisibility(View.GONE);
                decline.setVisibility(View.GONE);
                acceptDeclineLayout.setVisibility(View.VISIBLE);

                status_textview.setText("Request Accepted");
            } else if (requestModel.getStatus().equals("2")) {
                status_textview.setVisibility(View.VISIBLE);
                accept.setVisibility(View.GONE);
                decline.setVisibility(View.GONE);
                acceptDeclineLayout.setVisibility(View.VISIBLE);
                status_textview.setText("Request Declined");
            } else {
                acceptDeclineLayout.setVisibility(View.GONE);

                if (requestModel.getDeliveryBy().equals("1")) {
                    deliveryBy.setText("Delivery By: Restaurant");
                } else {
                    deliveryBy.setText("Delivery By: Delivery Expert");
                }

                preparationTime.setText("Preparation Time: " + requestModel.getPreparationTime() + " mins");

            }


        } else {
            infoText.setText("Restaurtant Info: ");
            acceptDeclineLayout.setVisibility(View.GONE);
            qr_code.setVisibility(View.VISIBLE);


            statusButton.setText("Accept Payment Method");

            if (requestModel.getStatus().equals("1")) {
                statusButton.setVisibility(View.VISIBLE);
            } else {
                statusButton.setVisibility(View.INVISIBLE);
            }

            if (requestModel.getDeliveryBy().equals("1")) {
                deliveryBy.setText("Delivery By: Restaurant");
            } else {
                deliveryBy.setText("Delivery By: Delivery Expert");
            }

            preparationTime.setText("Preparation Time: " + requestModel.getPreparationTime() + " mins");


            if (requestModel.getStatus().equals("3") || requestModel.getStatus().equals("4")) {
                Glide.with(context).load(requestModel.getQr_code_image()).into(qr_code);
            }

        }


        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new FoodAdapterCheckout(context, requestModel.getList()));
    }

}