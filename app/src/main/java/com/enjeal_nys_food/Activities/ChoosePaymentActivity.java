package com.enjeal_nys_food.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enjeal_nys_food.Adapters.FoodAdapterCheckout;
import com.enjeal_nys_food.Adapters.PrimaryFoodAdapter;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.ModelClasses.RequestModel;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.Util.ConnectivityReceiver;
import com.enjeal_nys_food.Util.Parameters;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.enjeal_nys_food.parser.AllAPIS;
import com.enjeal_nys_food.parser.GetAsyncPut;
import com.google.gson.JsonObject;
import com.google.zxing.WriterException;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import androidmads.library.qrgenearator.QRGSaver;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChoosePaymentActivity extends AppCompatActivity {
    ChoosePaymentActivity context;
    private SavePref savePref;
    @BindView(R.id.cash_on_delivery)
    Button cashOnDelivery;
    @BindView(R.id.mobile_money)
    Button mobileMoney;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.estimated_time)
    TextView estimated_time;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    String TAG = "GenerateQRCode";

    String name_r = "", image_r = "", phone_r = "",
            name_d = "", image_d = "", phone_d = "";
    RequestModel requestModel;


    String savePath = Environment.getExternalStorageDirectory().getPath() + "/QRCode/";
    Bitmap bitmap;
    QRGEncoder qrgEncoder;

    private String qr_code_image = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_payment);
        ButterKnife.bind(this);

        setToolbar();

        requestModel = getIntent().getExtras().getParcelable("data");

        context = ChoosePaymentActivity.this;
        savePref = new SavePref(context);

        estimated_time.setText("Estimated Delivery Time : "  + util.minuteToTime(Double.parseDouble(getIntent().getStringExtra("total_time"))));
        address.setText(requestModel.getLocation());
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new FoodAdapterCheckout(context, requestModel.getList()));

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Order Summery");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.cash_on_delivery, R.id.mobile_money})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cash_on_delivery:
                genrateQR(requestModel.getId());
                break;
            case R.id.mobile_money:
                break;
        }
    }

    public void ORDER_PAYMENT(String paymentMethod) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);

        if (!qr_code_image.isEmpty()) {
            final MediaType MEDIA_TYPE = qr_code_image.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(qr_code_image);
            formBuilder.addFormDataPart("qrImage", file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.ORDERID, requestModel.getId());
        formBuilder.addFormDataPart(Parameters.COST, requestModel.getPrice());
        formBuilder.addFormDataPart(Parameters.PAYMENTMETHOD, paymentMethod);//0=cash , 1=paypal , 2= stripe
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.FOODORDERPAYMENT, formBody, savePref.getAuthorization_key()) {

            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonMainobject.getJSONObject("body");

                            name_r = body.getJSONObject("restaurant").getString("name");
                            image_r = body.getJSONObject("restaurant").getString("image");
                            phone_r = body.getJSONObject("restaurant").optString("phone");

                            name_d = body.getJSONObject("deliveryBoy").optString("name");
                            image_d = body.getJSONObject("deliveryBoy").optString("image");
                            phone_d = body.getJSONObject("deliveryBoy").optString("phone");

                            SuccessAlert();
                        } else {
                            util.showToast(context, jsonMainobject.getString("message"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void SuccessAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Order Placed Successfully!")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*Intent intent = new Intent(context, AssignedActivity.class);
                        intent.putExtra("id_r", "");
                        intent.putExtra("name_r", name_r);
                        intent.putExtra("image_r", image_r);
                        intent.putExtra("phone_r", phone_r);

                        intent.putExtra("id_d", "");
                        intent.putExtra("name_d", name_d);
                        intent.putExtra("image_d", image_d);
                        intent.putExtra("phone_d", phone_d);
                        intent.putExtra("deliveryBy", requestModel.getDeliveryBy());
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                        finishAffinity();*/

                        Intent intent = new Intent(context, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                })
                /*  .setNegativeButton("No", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                      }
                  })*/.show();
    }

    private void genrateQR(String inputValue) {
        if (inputValue.length() > 0) {
            WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;

            qrgEncoder = new QRGEncoder(
                    inputValue, null,
                    QRGContents.Type.TEXT,
                    smallerDimension);

            boolean save;
            String result;

            try {
                bitmap = qrgEncoder.encodeAsBitmap();
                // qrImage.setImageBitmap(bitmap);
                save = QRGSaver.save(savePath, inputValue, bitmap, QRGContents.ImageType.IMAGE_JPEG);
                result = save ? "Image Saved" : "Image Not Saved";
                //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                if (save) {
                    qr_code_image = util.getPath(this, getImageUri(bitmap));
                    ORDER_PAYMENT("0");
                }


            } catch (WriterException e) {
                Log.v(TAG, e.toString());
            }
        } else {
            //edtValue.setError("Required");
        }
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


}