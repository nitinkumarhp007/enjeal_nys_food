package com.enjeal_nys_food.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "Authorization";
    public static final String AUTH_KEY = "auth_key";
    public static final String SECURITYKEY = "securitykey";
    public static final String DEVICE_TOKEN = "deviceToken";
    public static final String EMAIL = "email";
    public static final String ONLINE_STATUS = "onlineStatus";
    public static final String SOCIAL_TYPE = "soical_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "deviceType";
    public static final String RANDOM_NUMBER = "random_number";
    public static final String FIRST_NAME = "first_name";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String PAYPALID = "paypalId";
    public static final String COUNTRY = "country";
    public static final String OLD_PASSWORD = "oldPassword";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String IMAGE = "image";
    public static final String IMAGES = "images";

    public static final String FRIEND_ID = "friend_id";
    public static final String ORDERID = "orderId";
    public static final String MESSAGE = "message";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String CATEGORY_ID = "categoryId";
    public static final String FOODCATEGORY_ID = "foodCategoryId";
    public static final String RENT = "rent";
    public static final String DESCRIPTION = "description";
    public static final String PRODUCT_ID = "productId";
    public static final String RENT_FROM = "rent_from";
    public static final String RENT_TO = "rent_to";
    public static final String RENT_DAYS = "rent_days";
    public static final String BILL_AMOUNT = "bill_amount";
    public static final String BUYER_PAYMENT_JSON = "buyer_payment_json";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String IS_OFFER = "is_offer";
    public static final String OFFER_AMOUNT = "offer_amount";
    public static final String MESSAGE_ID = "message_id";
    public static final String OFFER_STATUS = "offer_status";
    public static final String STATUS = "status";
    public static final String REQUEST_ID = "request_id";
    public static final String REQUEST_ID_ = "Request_id";
    public static final String CARD_NO = "card_number";
    public static final String CARD_TYPE = "card_type";
    public static final String CARD_CVV = "card_cvv";
    public static final String EXPIRY_MONTH = "card_expiry_month";
    public static final String EXPIRY_YEAR = "card_expiry_year";

    public static final String CARD_NAME = "cardholdername";
    public static final String AMOUNT = "amount";
    public static final String CARD_ID = "card_id";
    public static final String CANCEL_BY = "cancel_by";
    public static final String PAYPAL_EMAIL = "paypal_email";
    public static final String RANGE = "range";
    public static final String DAILY = "daily";
    public static final String WEEKLY = "weekly";
    public static final String LOCATION = "location";
    public static final String STARTDATE = "startDate";
    public static final String STARTDATE_SHOW = "startDateShow";
    public static final String ENDDATE = "endDate";
    public static final String ENDDATE_SHOW = "endDateShow";
    public static final String RECEIVER_ID = "receiverId";
    public static final String TYPE = "type";
    public static final String THUMB_IMAGE = "thumb";
    public static final String CHAT_ID = "chat_id";
    public static final String USERID = "userId";
    public static final String PRICE = "price";
    public static final String INITIALCOST = "initialCost";
    public static final String DELIVERYCHARGES = "deliveryCharges";
    public static final String COMMSIION = "commission";
    public static final String TRANSACTIONNO = "transactionNo";
    public static final String TRANSACTIONJSON = "transactionJson";
    public static final String CITY = "city";
    public static final String ZIP = "zip";

    public static final String RATING = "rating";
    public static final String COMMENT = "comment";
    public static final String BOOKINGID = "bookingId";


    public static final String USERNAME = "username";
    public static final String ROLE = "role";
    public static final String ADDRESS = "address";
    public static final String RESTAURANTID = "restaurantId";
    public static final String STOCK = "stock";
    public static final String SIZES = "sizes";
    public static final String ID = "id";
    public static final String FOODMENUITEMIDS = "foodMenuItemIds";
    public static final String HOUSENUMBER = "houseNumber";
    public static final String STREETADDRESS = "streetAddress";
    public static final String ZIPCODE = "zipCode";
    public static final String ADDRESS_ID = "addressId";
    public static final String QUANTITY ="quantity" ;
    public static final String RESTAURTANTLICENSEIMAGE ="restaurantLicenseImage" ;
    public static final String DELIVERYEXPERTIDIMAGE ="deliveryExpertIdImage" ;
    public static final String PROMOTION_PRICE = "promotionPrice";
    public static final String PROMOTION_DATE = "promotionDate";
    public static final String PAYMENTMETHOD = "paymentMethod";
    public static final String DELIVERYTYPE = "deliveryType";
    public static final String ESTIMATEDDELIVERYTIME = "estimatedDeliveryTime";
    public static final String EXPECTEDTIMEDELIVERY = "expectedTimeDelivery";
    public static final String SPECIALITYONFOODORDER = "specialityOnFoodOrder";
    public static final String REASON ="reason" ;
    public static final String PREPARATION_TIME = "preparationTime";
    public static final String DELIVERYBY ="deliveryBy" ;
    public static final String COST ="cost" ;
}















