package com.enjeal_nys_food.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class AddressModel implements Parcelable {
    String id = "";
    String name = "";
    String mobile = "";
    String address_line_1 = "";
    String state = "";
    String city = "";
    String country = "";
    String pin_code = "";
    boolean is_select = false;

    public AddressModel() {
    }


    protected AddressModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        mobile = in.readString();
        address_line_1 = in.readString();
        state = in.readString();
        city = in.readString();
        country = in.readString();
        pin_code = in.readString();
        is_select = in.readByte() != 0;
    }

    public static final Creator<AddressModel> CREATOR = new Creator<AddressModel>() {
        @Override
        public AddressModel createFromParcel(Parcel in) {
            return new AddressModel(in);
        }

        @Override
        public AddressModel[] newArray(int size) {
            return new AddressModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public boolean isIs_select() {
        return is_select;
    }

    public void setIs_select(boolean is_select) {
        this.is_select = is_select;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public void setAddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(mobile);
        dest.writeString(address_line_1);
        dest.writeString(state);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(pin_code);
        dest.writeByte((byte) (is_select ? 1 : 0));
    }
}
