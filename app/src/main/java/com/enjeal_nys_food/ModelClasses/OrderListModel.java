package com.enjeal_nys_food.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderListModel implements Parcelable {
    String id="";
    String product_id="";
    String rent_from="";
    String rent_to="";
    String rent_days="";
    String bill_amount="";
    String product_name="";
    String product_image="";
    String created="";
    String status="";
    String description="";
    String over_cancel_period="";
    String added_by="";
    String is_rating_done="";
    String quantity="";

    String user_id = "";
    String user_name = "";
    String user_image = "";

    String address_name = "";
    String address_phone = "";
    String address_houseNumber = "";
    String address_streetAddress = "";
    String address_city = "";
    String address_country = "";
    String address_zipCode = "";


    public OrderListModel()
    {}


    protected OrderListModel(Parcel in) {
        id = in.readString();
        product_id = in.readString();
        rent_from = in.readString();
        rent_to = in.readString();
        rent_days = in.readString();
        bill_amount = in.readString();
        product_name = in.readString();
        product_image = in.readString();
        created = in.readString();
        status = in.readString();
        description = in.readString();
        over_cancel_period = in.readString();
        added_by = in.readString();
        is_rating_done = in.readString();
        quantity = in.readString();
        user_id = in.readString();
        user_name = in.readString();
        user_image = in.readString();
        address_name = in.readString();
        address_phone = in.readString();
        address_houseNumber = in.readString();
        address_streetAddress = in.readString();
        address_city = in.readString();
        address_country = in.readString();
        address_zipCode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(product_id);
        dest.writeString(rent_from);
        dest.writeString(rent_to);
        dest.writeString(rent_days);
        dest.writeString(bill_amount);
        dest.writeString(product_name);
        dest.writeString(product_image);
        dest.writeString(created);
        dest.writeString(status);
        dest.writeString(description);
        dest.writeString(over_cancel_period);
        dest.writeString(added_by);
        dest.writeString(is_rating_done);
        dest.writeString(quantity);
        dest.writeString(user_id);
        dest.writeString(user_name);
        dest.writeString(user_image);
        dest.writeString(address_name);
        dest.writeString(address_phone);
        dest.writeString(address_houseNumber);
        dest.writeString(address_streetAddress);
        dest.writeString(address_city);
        dest.writeString(address_country);
        dest.writeString(address_zipCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrderListModel> CREATOR = new Creator<OrderListModel>() {
        @Override
        public OrderListModel createFromParcel(Parcel in) {
            return new OrderListModel(in);
        }

        @Override
        public OrderListModel[] newArray(int size) {
            return new OrderListModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getRent_from() {
        return rent_from;
    }

    public void setRent_from(String rent_from) {
        this.rent_from = rent_from;
    }

    public String getRent_to() {
        return rent_to;
    }

    public void setRent_to(String rent_to) {
        this.rent_to = rent_to;
    }

    public String getRent_days() {
        return rent_days;
    }

    public void setRent_days(String rent_days) {
        this.rent_days = rent_days;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOver_cancel_period() {
        return over_cancel_period;
    }

    public void setOver_cancel_period(String over_cancel_period) {
        this.over_cancel_period = over_cancel_period;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getIs_rating_done() {
        return is_rating_done;
    }

    public void setIs_rating_done(String is_rating_done) {
        this.is_rating_done = is_rating_done;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getAddress_name() {
        return address_name;
    }

    public void setAddress_name(String address_name) {
        this.address_name = address_name;
    }

    public String getAddress_phone() {
        return address_phone;
    }

    public void setAddress_phone(String address_phone) {
        this.address_phone = address_phone;
    }

    public String getAddress_houseNumber() {
        return address_houseNumber;
    }

    public void setAddress_houseNumber(String address_houseNumber) {
        this.address_houseNumber = address_houseNumber;
    }

    public String getAddress_streetAddress() {
        return address_streetAddress;
    }

    public void setAddress_streetAddress(String address_streetAddress) {
        this.address_streetAddress = address_streetAddress;
    }

    public String getAddress_city() {
        return address_city;
    }

    public void setAddress_city(String address_city) {
        this.address_city = address_city;
    }

    public String getAddress_country() {
        return address_country;
    }

    public void setAddress_country(String address_country) {
        this.address_country = address_country;
    }

    public String getAddress_zipCode() {
        return address_zipCode;
    }

    public void setAddress_zipCode(String address_zipCode) {
        this.address_zipCode = address_zipCode;
    }
}
