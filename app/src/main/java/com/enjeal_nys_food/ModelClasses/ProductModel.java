package com.enjeal_nys_food.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductModel implements Parcelable {
    String id="";
    String name="";
    String description="";
    String weekly="";
    String image="";
    String userId="";
    String category="";
    String category_id="";
    String stock="";

    public ProductModel()
    {}

    protected ProductModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        weekly = in.readString();
        image = in.readString();
        userId = in.readString();
        category = in.readString();
        category_id = in.readString();
        stock = in.readString();
    }

    public static final Creator<ProductModel> CREATOR = new Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(weekly);
        dest.writeString(image);
        dest.writeString(userId);
        dest.writeString(category);
        dest.writeString(category_id);
        dest.writeString(stock);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeekly() {
        return weekly;
    }

    public void setWeekly(String weekly) {
        this.weekly = weekly;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public static Creator<ProductModel> getCREATOR() {
        return CREATOR;
    }
}
