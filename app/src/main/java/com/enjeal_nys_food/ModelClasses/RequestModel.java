package com.enjeal_nys_food.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class RequestModel implements Parcelable {
    String id = "";
    String username = "";
    String price = "";
    String Image = "";
    String status = "";
    String created = "";

    String user_id = "";
    String user_name = "";
    String user_image = "";
    String restaurant_id = "";
    String restaurant_name = "";
    String restaurant_image = "";
    String restaurant_address = "";
    String restaurant_lat = "";
    String restaurant_lng = "";
    String isRated = "";
    String specialRequest = "";
    String address_name = "";
    String address_phone = "";
    String address_houseNumber = "";
    String address_streetAddress = "";
    String address_city = "";
    String address_country = "";
    String address_zipCode = "";
    String estimatedDeliveryTime = "";
    String expectedTimeDelivery = "";
    String location = "";
    String latitude = "";
    String longitude = "";
    String qr_code_image = "";
    String preparationTime = "";
    String deliveryBy = "";
    String reason = "";
    String name_d = "";
    String phone_d = "";
    String image_d = "";
    String initialcost = "";
    String commission = "";
    String deliverycharges = "";
    ArrayList<MenuItemModel> list;

    public RequestModel() {
    }

    protected RequestModel(Parcel in) {
        id = in.readString();
        username = in.readString();
        price = in.readString();
        Image = in.readString();
        status = in.readString();
        created = in.readString();
        user_id = in.readString();
        user_name = in.readString();
        user_image = in.readString();
        restaurant_id = in.readString();
        restaurant_name = in.readString();
        restaurant_image = in.readString();
        restaurant_address = in.readString();
        restaurant_lat = in.readString();
        restaurant_lng = in.readString();
        isRated = in.readString();
        specialRequest = in.readString();
        address_name = in.readString();
        address_phone = in.readString();
        address_houseNumber = in.readString();
        address_streetAddress = in.readString();
        address_city = in.readString();
        address_country = in.readString();
        address_zipCode = in.readString();
        estimatedDeliveryTime = in.readString();
        expectedTimeDelivery = in.readString();
        location = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        qr_code_image = in.readString();
        preparationTime = in.readString();
        deliveryBy = in.readString();
        reason = in.readString();
        name_d = in.readString();
        phone_d = in.readString();
        image_d = in.readString();
        initialcost = in.readString();
        commission = in.readString();
        deliverycharges = in.readString();
        list = in.createTypedArrayList(MenuItemModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(username);
        dest.writeString(price);
        dest.writeString(Image);
        dest.writeString(status);
        dest.writeString(created);
        dest.writeString(user_id);
        dest.writeString(user_name);
        dest.writeString(user_image);
        dest.writeString(restaurant_id);
        dest.writeString(restaurant_name);
        dest.writeString(restaurant_image);
        dest.writeString(restaurant_address);
        dest.writeString(restaurant_lat);
        dest.writeString(restaurant_lng);
        dest.writeString(isRated);
        dest.writeString(specialRequest);
        dest.writeString(address_name);
        dest.writeString(address_phone);
        dest.writeString(address_houseNumber);
        dest.writeString(address_streetAddress);
        dest.writeString(address_city);
        dest.writeString(address_country);
        dest.writeString(address_zipCode);
        dest.writeString(estimatedDeliveryTime);
        dest.writeString(expectedTimeDelivery);
        dest.writeString(location);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(qr_code_image);
        dest.writeString(preparationTime);
        dest.writeString(deliveryBy);
        dest.writeString(reason);
        dest.writeString(name_d);
        dest.writeString(phone_d);
        dest.writeString(image_d);
        dest.writeString(initialcost);
        dest.writeString(commission);
        dest.writeString(deliverycharges);
        dest.writeTypedList(list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RequestModel> CREATOR = new Creator<RequestModel>() {
        @Override
        public RequestModel createFromParcel(Parcel in) {
            return new RequestModel(in);
        }

        @Override
        public RequestModel[] newArray(int size) {
            return new RequestModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getRestaurant_image() {
        return restaurant_image;
    }

    public void setRestaurant_image(String restaurant_image) {
        this.restaurant_image = restaurant_image;
    }

    public String getRestaurant_address() {
        return restaurant_address;
    }

    public void setRestaurant_address(String restaurant_address) {
        this.restaurant_address = restaurant_address;
    }

    public String getRestaurant_lat() {
        return restaurant_lat;
    }

    public void setRestaurant_lat(String restaurant_lat) {
        this.restaurant_lat = restaurant_lat;
    }

    public String getRestaurant_lng() {
        return restaurant_lng;
    }

    public void setRestaurant_lng(String restaurant_lng) {
        this.restaurant_lng = restaurant_lng;
    }

    public String getIsRated() {
        return isRated;
    }

    public void setIsRated(String isRated) {
        this.isRated = isRated;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    public String getAddress_name() {
        return address_name;
    }

    public void setAddress_name(String address_name) {
        this.address_name = address_name;
    }

    public String getAddress_phone() {
        return address_phone;
    }

    public void setAddress_phone(String address_phone) {
        this.address_phone = address_phone;
    }

    public String getAddress_houseNumber() {
        return address_houseNumber;
    }

    public void setAddress_houseNumber(String address_houseNumber) {
        this.address_houseNumber = address_houseNumber;
    }

    public String getAddress_streetAddress() {
        return address_streetAddress;
    }

    public void setAddress_streetAddress(String address_streetAddress) {
        this.address_streetAddress = address_streetAddress;
    }

    public String getAddress_city() {
        return address_city;
    }

    public void setAddress_city(String address_city) {
        this.address_city = address_city;
    }

    public String getAddress_country() {
        return address_country;
    }

    public void setAddress_country(String address_country) {
        this.address_country = address_country;
    }

    public String getAddress_zipCode() {
        return address_zipCode;
    }

    public void setAddress_zipCode(String address_zipCode) {
        this.address_zipCode = address_zipCode;
    }

    public String getEstimatedDeliveryTime() {
        return estimatedDeliveryTime;
    }

    public void setEstimatedDeliveryTime(String estimatedDeliveryTime) {
        this.estimatedDeliveryTime = estimatedDeliveryTime;
    }

    public String getExpectedTimeDelivery() {
        return expectedTimeDelivery;
    }

    public void setExpectedTimeDelivery(String expectedTimeDelivery) {
        this.expectedTimeDelivery = expectedTimeDelivery;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getQr_code_image() {
        return qr_code_image;
    }

    public void setQr_code_image(String qr_code_image) {
        this.qr_code_image = qr_code_image;
    }

    public String getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(String preparationTime) {
        this.preparationTime = preparationTime;
    }

    public String getDeliveryBy() {
        return deliveryBy;
    }

    public void setDeliveryBy(String deliveryBy) {
        this.deliveryBy = deliveryBy;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getName_d() {
        return name_d;
    }

    public void setName_d(String name_d) {
        this.name_d = name_d;
    }

    public String getPhone_d() {
        return phone_d;
    }

    public void setPhone_d(String phone_d) {
        this.phone_d = phone_d;
    }

    public String getImage_d() {
        return image_d;
    }

    public void setImage_d(String image_d) {
        this.image_d = image_d;
    }

    public String getInitialcost() {
        return initialcost;
    }

    public void setInitialcost(String initialcost) {
        this.initialcost = initialcost;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getDeliverycharges() {
        return deliverycharges;
    }

    public void setDeliverycharges(String deliverycharges) {
        this.deliverycharges = deliverycharges;
    }

    public ArrayList<MenuItemModel> getList() {
        return list;
    }

    public void setList(ArrayList<MenuItemModel> list) {
        this.list = list;
    }
}
