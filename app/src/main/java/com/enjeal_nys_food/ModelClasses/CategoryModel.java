package com.enjeal_nys_food.ModelClasses;

import java.util.ArrayList;

public class CategoryModel {
    String id = "";
    String name = "";
    String image = "";
    ArrayList<MenuItemModel> list;


    public ArrayList<MenuItemModel> getList() {
        return list;
    }

    public void setList(ArrayList<MenuItemModel> list) {
        this.list = list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
