package com.enjeal_nys_food.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class MenuItemModel implements Parcelable {
    String id = "";
    String name = "";
    String image = "";
    String price = "";
    String description = "";
    String category_id = "";
    String category_name = "";
    String quantity = "";
    String promotionPrice = "";
    String promotionDate = "";
    String promotionValid = "";
    boolean is_select = false;

    public MenuItemModel() {
    }


    protected MenuItemModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        image = in.readString();
        price = in.readString();
        description = in.readString();
        category_id = in.readString();
        category_name = in.readString();
        quantity = in.readString();
        promotionPrice = in.readString();
        promotionDate = in.readString();
        promotionValid = in.readString();
        is_select = in.readByte() != 0;
    }

    public static final Creator<MenuItemModel> CREATOR = new Creator<MenuItemModel>() {
        @Override
        public MenuItemModel createFromParcel(Parcel in) {
            return new MenuItemModel(in);
        }

        @Override
        public MenuItemModel[] newArray(int size) {
            return new MenuItemModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public String getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(String promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public String getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(String promotionDate) {
        this.promotionDate = promotionDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getPromotionValid() {
        return promotionValid;
    }

    public void setPromotionValid(String promotionValid) {
        this.promotionValid = promotionValid;
    }

    public String getName() {
        return name;
    }

    public boolean isIs_select() {
        return is_select;
    }

    public void setIs_select(boolean is_select) {
        this.is_select = is_select;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(price);
        dest.writeString(description);
        dest.writeString(category_id);
        dest.writeString(category_name);
        dest.writeString(quantity);
        dest.writeString(promotionPrice);
        dest.writeString(promotionDate);
        dest.writeString(promotionValid);
        dest.writeByte((byte) (is_select ? 1 : 0));
    }
}
