package com.enjeal_nys_food.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.enjeal_nys_food.DeliveryExpertHomeActivity;
import com.enjeal_nys_food.MainActivity;
import com.enjeal_nys_food.R;
import com.enjeal_nys_food.RestaurtantMainActivity;
import com.enjeal_nys_food.Util.SavePref;
import com.enjeal_nys_food.Util.util;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SavePref savePref;
    private static int i;
    String message = "", friend_id = "", order_id = "", message_id = "", productId = "", product_name = "", notification_code = "", product_id = "", username = "", sender_id = "", chat_message, timeStamp;
    String id_r = "", name_r = "", image_r = "", phone_r = "",
            id_d = "", name_d = "",
            image_d = "", phone_d = "", deliveryBy = "";

    String CHANNEL_ID = "";// The id of the channel.
    String CHANNEL_ONE_NAME = "Channel One";
    NotificationChannel notificationChannel;
    NotificationManager notificationManager;
    Notification notification;
    String groupId = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject obj = null;
        savePref = new SavePref(getApplicationContext());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData());
        message = remoteMessage.getData().get("title");
        notification_code = remoteMessage.getData().get("type");

        getManager();
        CHANNEL_ID = getApplicationContext().getPackageName();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        }

        if (notification_code.equals("1000000000000")) {//chat
            try {
                JSONObject body1 = new JSONObject(remoteMessage.getData().get("body"));
                JSONObject object = body1.getJSONObject("chatMessage");
                String message123 = object.getString("message");
                //message = message123;

                String created = object.getString("createdAt");
                String type = object.getString("type");
                sender_id = object.getString("senderId");
                message_id = object.getString("id");
                productId = object.getJSONObject("product").getString("id");
                product_name = object.getJSONObject("product").getString("name");
                username = object.getJSONObject("otherUser").optString("name");
                Log.e("chat_idp", savePref.getCHAT_ID());
                if (savePref.getChatScreen() && savePref.getCHAT_ID().equals(productId)) {
                    publishResultsMessage(message_id, type, message123, created, username, sender_id);
                } else {
                    sendNotification(getApplicationContext(), message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (notification_code.equals("2")) {
            sendNotification(getApplicationContext(), message);
        } else if (notification_code.equals("7")) {
            JSONObject body1 = null;
            try {
                body1 = new JSONObject(remoteMessage.getData().get("body"));
                JSONObject restaurant = body1.getJSONObject("order").getJSONObject("restaurant");
                id_r = restaurant.getString("id");
                name_r = restaurant.getString("name");
                image_r = restaurant.getString("image");
                phone_r = restaurant.optString("phone");

                deliveryBy = body1.getJSONObject("order").getString("deliveryBy");

                if (!deliveryBy.equals("1")) {
                    JSONObject deliveryExpert = body1.getJSONObject("order").getJSONObject("deliveryExpert");
                    id_d = deliveryExpert.getString("id");
                    name_d = deliveryExpert.getString("name");
                    image_d = deliveryExpert.getString("image");
                    phone_d = deliveryExpert.optString("phone");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendNotification(getApplicationContext(), message);
        } else {
            sendNotification(getApplicationContext(), message);
        }
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        SavePref.setDeviceToken(getApplicationContext(), "token", token);
        Log.e("token___", token);
    }

    //2= user : accept
    //3= user : decline
    //5= picked up (user)
    //7=delivered (User)

    //1= Restorant : Request
    //33= Restorant : Booking done
    //6=picked up (restaurant)
    //8=delivered (restaurant)


    //4= You have new food order. (Delivery Boy )


    private void sendNotification(Context context, String message) {
        Intent intent = null;
        PendingIntent pendingIntent;
        if (notification_code.equals("1") || notification_code.equals("33") || notification_code.equals("6") || notification_code.equals("8")) {
            intent = new Intent(context, RestaurtantMainActivity.class);//restaurant
        } else if (notification_code.equals("2") || notification_code.equals("3")
                || notification_code.equals("5") || notification_code.equals("7")) {
            intent = new Intent(context, MainActivity.class); //user
            intent.putExtra("id_r", id_r);
            intent.putExtra("name_r", name_r);
            intent.putExtra("image_r", image_r);
            intent.putExtra("phone_r", phone_r);
            intent.putExtra("id_d", id_d);
            intent.putExtra("name_d", name_d);
            intent.putExtra("image_d", image_d);
            intent.putExtra("phone_d", phone_d);
            intent.putExtra("deliveryBy", deliveryBy);

        } else if (notification_code.equals("4")) {
            intent = new Intent(context, DeliveryExpertHomeActivity.class);
        }
        intent.putExtra("message", message);
        intent.putExtra("notification_code", notification_code);
        intent.putExtra("is_from_push", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

       /* Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setOngoing(false)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);*/


        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(context.getResources().getString(R.string.app_name))
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

    /*    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.noti_logo).setLargeIcon(icon1);;
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);


    }

    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    // offer_status
    private void publishResultsMessage(String message_id, String type, String message, String created, String username, String sender_id) {

        Intent intent = new Intent(util.NOTIFICATION_MESSAGE);
        intent.putExtra("message", message);
        intent.putExtra("created", created);
        intent.putExtra("username", username);
        intent.putExtra("sender_id", sender_id);
        intent.putExtra("type", type);
        intent.putExtra("message_id", message_id);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
