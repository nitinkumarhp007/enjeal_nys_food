package com.enjeal_nys_food;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.enjeal_nys_food.Fragments.User.DeliveryBoy.HomeDeliveryExpertFragment;
import com.enjeal_nys_food.Fragments.User.DeliveryBoy.ProfileDeliveryFragment;
import com.enjeal_nys_food.Fragments.User.ProfileUserFragment;
import com.enjeal_nys_food.Util.GPSTracker;
import com.enjeal_nys_food.Util.SavePref;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeliveryExpertHomeActivity extends AppCompatActivity {
    DeliveryExpertHomeActivity context;

    private SavePref savePref;
    BottomNavigationView navigation;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.setting)
    SwitchCompat setting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_expert_home);
        context = DeliveryExpertHomeActivity.this;
        ButterKnife.bind(this);

        savePref = new SavePref(context);

        Log.e("auth_key", savePref.getID());
        //  toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        navigation.setSelectedItemId(R.id.navigation_home);



        Log.e("token____", SavePref.getDeviceToken(this, "token"));

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            setting.setVisibility(View.INVISIBLE);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    title.setText("Orders");
                    loadFragment(new HomeDeliveryExpertFragment());
                    return true;
                case R.id.navigation_profile:
                    setting.setVisibility(View.INVISIBLE);
                    loadFragment(new ProfileDeliveryFragment());
                    title.setText("Profile");
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }


}

